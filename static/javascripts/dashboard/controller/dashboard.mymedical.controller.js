/**
 * Created by IDCS12 on 6/4/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyconditionController', MyconditionControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyconditionsoverviewController', MyconditionsoverviewControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MymedicationsController', MymedicationsControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MymedicationsoverviewController', MymedicationsoverviewControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyproceduresController', MyproceduresControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyproceduresoverviewController', MyproceduresoverviewControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyallergiesController', MyallergiesControllerFun);

    angular.module('myApp.dashboard.mymedical.controller')
        .controller('MyallergiesoverviewController', MyallergiesoverviewControllerFun);


    MyconditionControllerFun.$inject =['$scope', '$http', '$stateParams', '$state', '$filter', '$window', 'ScrollService', 'myConditionService'];
    MyconditionsoverviewControllerFun.$inject =['$scope', '$http', '$rootScope', '$interval', '$timeout', '$stateParams', '$state', '$window', 'ScrollService', 'ConditionsService', 'CacheService', 'myConditionOverviewService', '$mdDialog', 'UpdateMyAllMedicalInformation'];

    MymedicationsControllerFun.$inject =['$scope', '$http', '$stateParams', '$state', '$filter', '$window', 'ScrollService','myMedicationService'];
    MymedicationsoverviewControllerFun.$inject =['$scope', '$http', '$rootScope', '$interval', '$timeout', '$stateParams', '$state', '$window', 'ScrollService', 'MedicationsService', 'CacheService', 'myMedicationsOverviewService', '$mdDialog', 'UpdateMyAllMedicalInformation'];

    MyproceduresControllerFun.$inject =['$scope', '$http', '$stateParams', '$state', '$filter', '$window', 'ScrollService', 'myProcedureService'];
    MyproceduresoverviewControllerFun.$inject =['$scope', '$http', '$rootScope', '$interval', '$timeout', '$stateParams', '$state', '$window', 'ScrollService', 'ProceduresService', 'CacheService', 'myProcedureOverviewService', '$mdDialog', 'UpdateMyAllMedicalInformation'];

    MyallergiesControllerFun.$inject =['$scope', '$http', '$stateParams', '$state', '$filter', '$window', 'ScrollService','MyAllergyService'];
    MyallergiesoverviewControllerFun.$inject =['$scope', '$http', '$rootScope', '$stateParams', '$state', '$window', 'ScrollService', 'myAllergyOverviewService', '$mdDialog'];


    function MyconditionControllerFun($scope, $http, $stateParams, $state, $filter, $window, ScrollService, myConditionService, CacheService){

        var vm = this;
        vm.isLoading=true;
        vm.isFilterMyCondition = false;
        vm.isNameAccending = false;
        vm.isDateAccending = false;
        vm.conditionsName = [];


        myConditionService.all().success(function(response){
            vm.conditionsName = response;
            vm.isLoading = false;
        }).error(function(error, status){
            $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myConditionControlfun'+error.detail + status };
            vm.isLoading = false;
        });


        $scope.$on("MyConditionUpdateEvent", function(event) {
            vm.isLoading=true;
            myConditionService.all().success(function(response){
                vm.conditionsName = response;
                vm.isLoading = false;
            }).error(function(){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myConditionControlfun'+error.detail + status };
                vm.isLoading = false;
            });
        });

        vm.filterMyCondition = function()
        {
            vm.isFilterMyCondition = !vm.isFilterMyCondition;
        }

        vm.filterByName = function()
        {
            vm.isNameAccending = !vm.isNameAccending;
            vm.conditionsName = $filter("orderBy")(vm.conditionsName, 'condition', vm.isNameAccending);
        }
        vm.filterByDate = function()
        {
            vm.isDateAccending = !vm.isDateAccending;
            vm.conditionsName = $filter("orderBy")(vm.conditionsName, 'date', vm.isDateAccending);
        }
        
        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }


    function MyconditionsoverviewControllerFun($scope, $http, $rootScope,  $interval, $timeout, $stateParams, $state, $window, ScrollService, ConditionsService, CacheService, myConditionOverviewService, $mdDialog, UpdateMyAllMedicalInformation){
        var vm = this;
        vm.isLoading=true;
        vm.conditionList = [];
        vm.isFilter=false;
        vm.isNewCondition = false;
        vm.myConditionFilterOrForm = true;
        var isloaded=true;
        var timeout, interval;
        var d=new Date();
        vm.formName ="myCondition_Form";
        vm.myConditionFormData = {
            patient: null,
            condition: null,
            date: null,
            doctor: null,
            note: null,
            today : d.getFullYear()+"-"+(d.getMonth()>9?(d.getMonth()+1):"0"+(d.getMonth()+1))+"-"+(d.getDate()>9?d.getDate():"0"+d.getDate())
        };

        vm.oriMyCondition = angular.copy(vm.myConditionFormData);

        vm.clearForm = function(myConditionForm){
            vm.myConditionFormData.date = vm.myConditionFormData.doctor = vm.myConditionFormData.note = null;
        };

        if($stateParams.myConditionId !='addCondition'){
            vm.myConditionFilterOrForm = false;
            myConditionOverviewService.search($stateParams.myConditionId).success(function(response){
                vm.myConditionFormData.patient  = response.patient;
                vm.myConditionFormData.condition = response.condition;
                vm.myConditionFormData.date = response.date;
                vm.myConditionFormData.doctor = response.doctor;
                vm.myConditionFormData.note = response.note;
                vm.isLoading = false;
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            });


        }else{
            vm.isLoading =false;
            vm.isNewCondition =true;
            if($stateParams.myConditionName!=null)
            {
                vm.myConditionFormData.condition = $stateParams.myConditionName;
                vm.myConditionFilterOrForm = false;
            }
        }

        vm.filterListChange = function(filterList){
            vm.isFilter = true;
            vm.conditionList = [];

            if(filterList != ""){
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout = $timeout(function(){
                    vm.m    = true;
                    vm.isFilter =true;
                    ConditionsService.filterService(filterList).success(function(response){
                        vm.conditionList =response.results;
                        vm.isDisabled =false;
                        vm.isLoading =false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }
            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("conditions_cache")) {
                var data = CacheService.getCache("conditions_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){
                    vm.conditionList.push(data.list[i]);
                    i++;
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }
                }, 10);
                vm.after = data.after;
                }
            }
        };



        vm.updateMyCondition = function(){
            vm.isLoading = true;
            myConditionOverviewService.update(vm.myConditionFormData, $stateParams.myConditionId).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyConditionUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myconditionsoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myConditionFormData.condition + ' update'};
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            })
        };

        vm.showConfirm = function(ev) {
            var confirm = $mdDialog.confirm()
              /*.parent(angular.element(document.body))*/
              .title('Would you like to delete your condition?')
              .content('all of the records of this condition will be deleted')
              .ariaLabel('Lucky day')
              .ok('Please do it!')
              .cancel('Please don\'t do it !')
              .targetEvent(ev);

            $mdDialog.show(confirm).then(function() {
                $scope.alert = 'You decided to get rid of your debt.';
                vm.isLoading = true;
                myConditionOverviewService.remove($stateParams.myConditionId).success(function(){
                    vm.isLoading = false;
                    UpdateMyAllMedicalInformation.allUpdate();
                    $rootScope.$broadcast("MyConditionUpdateEvent");
                    $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myconditionsoverview")));
                    $scope.alerts[0]={ type: 'success', msg: vm.myConditionFormData.condition + ' successfully deleted'+error.detail + status };

                }).error(function(error, status){
                   $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong while deleting condition '+error.detail + status };
                    vm.isLoading=false;
                })
            }, function() {
              $scope.alerts[0]={ type: 'danger', msg: vm.myConditionFormData.condition +  ' delete task aborted' };
            });
          };

        /*vm.deleteMyCondition = function(){
            vm.isLoading = true;
            alert("Do u want to delete? ");
            myConditionOverviewService.remove($stateParams.myConditionId).success(function(){
                vm.isLoading = false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyConditionUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myconditionsoverview")));
                $scope.alerts[0]={ type: 'danger', msg: 'successfully delete condition '+error.detail + status };

            }).error(function(error, status){
               $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong while deleting condition '+error.detail + status };
                vm.isLoading=false;
            })
        };*/

        /*for saving the condition and sending the id to the api*/
        vm.sendMyCondition = function(conditionName){
            vm.myConditionFormData.condition = conditionName;
            vm.myConditionFilterOrForm = false;
        };
        vm.submitMyCondition = function(){
            myConditionOverviewService.save(vm.myConditionFormData).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyConditionUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myconditionsoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myConditionFormData.condition + ' Condition saved'};
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong while saving the condition '+error.detail + status };
                vm.isLoading=false;
            });

            vm.myConditionFilterOrForm = false;
        };
        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myconditionsoverview")));
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }


    function MymedicationsControllerFun($scope, $http, $stateParams, $state, $filter, $window, ScrollService, myMedicationService){

        var vm = this;
        vm.isLoading=true;
        vm.isFilterMyMedication = false;
        vm.isNameAccending = false;
        vm.isDateAccending = false;
        vm.medicationsName = [];


        myMedicationService.all().success(function(response){
            console.log("frequncy comes "+ response);
            vm.medicationsName = response;
            vm.isLoading = false;
        }).error(function(){
            $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myConditionControlfun'+error.detail + status };
            vm.isLoading = false;
        });

        $scope.$on("MyMedicationUpdateEvent", function(event) {
            vm.isLoading=true;
            myMedicationService.all().success(function(response){
                vm.medicationsName = response;
                vm.isLoading = false;
            }).error(function(){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myConditionControlfun'+error.detail + status };
                vm.isLoading = false;
            });
        });


        vm.filterMyMedication = function()
        {
            vm.isFilterMyMedication = !vm.isFilterMyMedication;
        }

        vm.filterByName = function()
        {
            vm.isNameAccending = !vm.isNameAccending;
            vm.medicationsName = $filter("orderBy")(vm.medicationsName, 'salt', vm.isNameAccending);
        }
        vm.filterByDate = function()
        {
            vm.isDateAccending = !vm.isDateAccending;
            vm.medicationsName = $filter("orderBy")(vm.medicationsName, 'date', vm.isDateAccending);
        }

        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }


    function MymedicationsoverviewControllerFun($scope, $http, $rootScope, $interval, $timeout, $stateParams, $state, $window, ScrollService, MedicationsService, CacheService, myMedicationsOverviewService, $mdDialog, UpdateMyAllMedicalInformation){
        var vm = this;
        vm.isLoading=true;
        vm.unitsStandard = '';
        vm.methods = '';

        vm.medicationsList = [];
        vm.isFilter=false;
        vm.isNewMedication = false;
        vm.myMedicationFilterOrForm = true;
        var isloaded=true;
        var timeout, interval;
        var d=new Date();

        vm.myMedicationFormData = {
            salt : null,
            amount : null,
            unit : null,
            method : null,
            date : null,
            note : null,
            doctor : null,
            frequency : null,
            today : d.getFullYear()+"-"+(d.getMonth()>9?(d.getMonth()+1):"0"+(d.getMonth()+1))+"-"+(d.getDate()>9?d.getDate():"0"+d.getDate())
        };

        vm.oriMyMedication = angular.copy(vm.myMedicationFormData);

        myMedicationsOverviewService.unit().success(function(response){
            vm.unitsStandard = response;
        }).error(function(error, status){
           $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
        });

        myMedicationsOverviewService.method().success(function(response){
            vm.methods = response;
        }).error(function(error, status){
           $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
        });


        if( $stateParams.myMedicationId !='addMedication'){
            vm.myMedicationFilterOrForm = false;
            myMedicationsOverviewService.search($stateParams.myMedicationId).success(function(response){
                vm.myMedicationFormData.salt = response.salt;
                vm.myMedicationFormData.amount = response.amount;
                vm.myMedicationFormData.unit = response.unit;
                vm.myMedicationFormData.method = response.method;
                vm.myMedicationFormData.date = response.date;
                vm.myMedicationFormData.note = response.note;
                vm.myMedicationFormData.doctor = response.doctor;
                vm.myMedicationFormData.frequency = response.frequency;
                vm.isLoading =false;
            }).error(function(error, status){
               $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            })
        }else{
            vm.isLoading = false;
            vm.isNewMedication = true;
            if($stateParams.myMedicationName!=null)
            {
                vm.myMedicationFormData.salt = $stateParams.myMedicationName;
                vm.myMedicationFilterOrForm =false;
            }
        }


        vm.filterListChange=function(filterList){
            vm.isFilter=true;
            vm.medicationList=[];

            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    MedicationsService.filterService(filterList).success(function (response) {
                        vm.medicationList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("medications_cache")) {
                var data = CacheService.getCache("medications_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){

                    vm.medicationList.push(data.list[i]);
                    i++;

                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }

                }, 10);
                vm.after = data.after;
                }
            }
        };


        vm.clearForm = function(){
            vm.myMedicationFormData.amount = vm.myMedicationFormData.unit = vm.myMedicationFormData.method = vm.myMedicationFormData.date = vm.myMedicationFormData.note = vm.myMedicationFormData.doctor = vm.myMedicationFormData.frequency = null;
        };



        vm.updateMyMedication = function(){
            vm.isLoading = true;
            myMedicationsOverviewService.update(vm.myMedicationFormData, $stateParams.myMedicationId).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyMedicationUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".mymedicationsoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myMedicationFormData.salt +' Medication updated' };
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            })
        };

        vm.showConfirm = function(ev) {
            var confirm = $mdDialog.confirm()
              /*.parent(angular.element(document.body))*/
              .title('Would you like to delete your medication?')
              .content('all of the records of this medication will be deleted')
              .ariaLabel('Lucky day')
              .ok('Please do it!')
              .cancel('Please don\'t do it !')
              .targetEvent(ev);

            $mdDialog.show(confirm).then(function() {
                vm.isLoading = true;
                myMedicationsOverviewService.remove($stateParams.myMedicationId).success(function(){
                    vm.isLoading = false;
                    UpdateMyAllMedicalInformation.allUpdate();
                    $rootScope.$broadcast("MyMedicationUpdateEvent");
                    $state.go($state.current.name.substring(0, $state.current.name.indexOf(".mymedicationsoverview")));
                    $scope.alerts[0]={ type: 'success', msg: vm.myMedicationFormData.salt +' successfully deleted' };
                }).error(function(error, status){
                   $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong while deleting medication '+error.detail + status };
                    vm.isLoading=false;
                });
            }, function() {
              $scope.alerts[0]={ type: 'danger', msg: vm.myMedicationFormData.salt +  ' delete task aborted' };
            });
          };

        /*vm.deleteMyMedication = function(){
            vm.isLoading = true;
            myMedicationsOverviewService.remove($stateParams.myMedicationId).success(function(){
                vm.isLoading = false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyMedicationUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".mymedicationsoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myMedicationFormData.salt +' Medication deleted' };
            }).error(function(error, status){
               $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading=false;
            })
        };*/

        vm.sendMyMedication = function(medicationName){
            vm.myMedicationFormData.salt = medicationName;
            vm.myMedicationFilterOrForm =false;
        };

        vm.submitMyMedication =function(){
            myMedicationsOverviewService.save(vm.myMedicationFormData).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyMedicationUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".mymedicationsoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myMedicationFormData.salt + ' Medication saved'};
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading=false;
            });

            vm.myMedicationFilterOrForm = false;
        };
        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".mymedicationsoverview")));
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }



    function MyproceduresControllerFun($scope, $http, $stateParams, $state, $filter, $window, ScrollService, myProcedureService){

        var vm = this;
        vm.isLoading=true;
        vm.isFilterMyProcedure = false;
        vm.isNameAccending = false;
        vm.isDateAccending = false;
        vm.proceduresName = [];


        myProcedureService.all().success(function(response){
            vm.proceduresName = response;
            vm.isLoading = false;
        }).error(function(error, status){
            $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myProcedureControlfun'+error.detail + status };
            vm.isLoading = false;
        });

        $scope.$on("MyProcedureUpdateEvent", function(event) {
            vm.isLoading=true;
            myProcedureService.all().success(function(response){
                vm.proceduresName = response;
                vm.isLoading = false;
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in myProcedureControlfun'+error.detail + status };
                vm.isLoading = false;
            });
        });

        vm.filterMyProcedure = function()
        {
            vm.isFilterMyProcedure = !vm.isFilterMyProcedure;
        }

        vm.filterByName = function()
        {
            vm.isNameAccending = !vm.isNameAccending;
            vm.proceduresName = $filter("orderBy")(vm.proceduresName, 'procedure', vm.isNameAccending);
        }
        vm.filterByDate = function()
        {
            vm.isDateAccending = !vm.isDateAccending;
            vm.proceduresName = $filter("orderBy")(vm.proceduresName, 'date', vm.isDateAccending);
        }

        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }


    function MyproceduresoverviewControllerFun($scope, $http, $rootScope, $interval, $timeout, $stateParams, $state, $window, ScrollService, ProceduresService, CacheService, myProcedureOverviewService, $mdDialog, UpdateMyAllMedicalInformation){
        var vm = this;
        vm.isLoading=true;
        vm.conditionList = [];
        vm.isFilter=false;
        vm.isNewProcedure = false;
        vm.myProcedureFilterOrForm = true;
        var isloaded=true;
        var timeout, interval;
        var d=new Date();

        vm.myProcedureFormData = {
            patient: null,
            procedure: null,
            date: null,
            doctor: null,
            note: null,
            today : d.getFullYear()+"-"+(d.getMonth()>9?(d.getMonth()+1):"0"+(d.getMonth()+1))+"-"+(d.getDate()>9?d.getDate():"0"+d.getDate())
        };

        vm.oriMyProcedure = angular.copy(vm.myProcedureFormData);

        if( $stateParams.myProcedureId !='addProcedure') {
            vm.myProcedureFilterOrForm = false;
            myProcedureOverviewService.search($stateParams.myProcedureId).success(function(response){
                vm.myProcedureFormData.patient  = response.patient;
                vm.myProcedureFormData.procedure = response.procedure;
                vm.myProcedureFormData.date = response.date;
                vm.myProcedureFormData.doctor = response.doctor;
                vm.myProcedureFormData.note = response.note;

                vm.isLoading = false;
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            });
        }else{
            vm.isLoading =false;
            vm.isNewProcedure =true;
            if($stateParams.myProcedureName!=null)
            {
                vm.myProcedureFormData.procedure = $stateParams.myProcedureName;
                vm.myProcedureFilterOrForm = false;
            }
        }

        vm.filterListChange=function(filterList){
            vm.isFilter=true;
            vm.procedureList=[];
            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    ProceduresService.filterService(filterList).success(function (response) {
                        vm.procedureList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("procedures_cache")) {
                var data = CacheService.getCache("procedures_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){

                    vm.procedureList.push(data.list[i]);
                    i++;

                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }

                }, 10);
                vm.after = data.after;
                }
            }
        };




        vm.clearForm = function(){
            vm.myProcedureFormData.date = vm.myProcedureFormData.doctor = vm.myProcedureFormData.note = null;
        };


        vm.updateMyProcedure = function(){
            vm.isLoading = true;
            myProcedureOverviewService.update(vm.myProcedureFormData, $stateParams.myProcedureId).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyProcedureUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myproceduresoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myProcedureFormData.procedure + ' Procedure update' };
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: ' something gone wrong'+error.detail + status };
                vm.isLoading = false;
            })
        };

        vm.showConfirm = function(ev) {
            var confirm = $mdDialog.confirm()
              /*.parent(angular.element(document.body))*/
              .title('Would you like to delete your procedure?')
              .content('all of the records of this procedure will be deleted')
              .ariaLabel('Lucky day')
              .ok('Please do it!')
              .cancel('Please don\'t do it !')
              .targetEvent(ev);

            $mdDialog.show(confirm).then(function() {
                vm.isLoading = true;
                myProcedureOverviewService.remove($stateParams.myProcedureId).success(function(){
                    vm.isLoading = false;
                    UpdateMyAllMedicalInformation.allUpdate();
                    $rootScope.$broadcast("MyProcedureUpdateEvent");
                    $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myproceduresoverview")));
                    $scope.alerts[0]={ type: 'success', msg: vm.myProcedureFormData.procedure +  ' Procedure delete' };
                }).error(function(error, status){
                   $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong '+error.detail + status };
                    vm.isLoading=false;
                })
            }, function() {
              $scope.alerts[0]={ type: 'danger', msg: vm.myProcedureFormData.procedure +  ' delete task aborted' };
            });
        };

        /*vm.deleteMyProcedure = function(){
            vm.isLoading = true;
            myProcedureOverviewService.remove($stateParams.myProcedureId).success(function(){
                vm.isLoading = false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyProcedureUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myproceduresoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myProcedureFormData.procedure + 'Procedure delete' };
            }).error(function(error, status){
               $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading=false;
            })
        };*/

        /*for saving the procedure and sending the id to the api*/
        vm.sendMyProcedure = function(procedureName){
            vm.myProcedureFormData.procedure = procedureName;
            vm.myProcedureFilterOrForm = false;
        };

        vm.submitMyProcedure = function(){
            myProcedureOverviewService.save(vm.myProcedureFormData).success(function(){
                vm.isLoading =false;
                UpdateMyAllMedicalInformation.allUpdate();
                $rootScope.$broadcast("MyProcedureUpdateEvent");
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myproceduresoverview")));
                $scope.alerts[0]={ type: 'success', msg: vm.myProcedureFormData.procedure + ' Procedure saved'};
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading=false;
            });

            vm.myProcedureFilterOrForm = false;
        };

        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".myproceduresoverview")));
            //$state.go("dashboard.myprocedures");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }


    function MyallergiesControllerFun($scope, $http, $stateParams, $state, $filter, $window, ScrollService, MyAllergyService){

        var vm = this;
        vm.isLoading=true;
        vm.isFilterMyAllergie = false;
        vm.isNameAccending = false;
        vm.isDateAccending = false;
        vm.allergiesList = [];


        MyAllergyService.allAllergy().success(function(response){
            //alert("geetin all allergies" + response);
            vm.allergiesList =response;
            vm.isLoading =false;
        }).error(function(error){
            alert("Nothing get" + error.details);
            vm.isLoading =false;
        });

        $scope.$on("MyAllergyUpdateEvent", function(event){
            vm.isLoading=true;
            MyAllergyService.allAllergy().success(function(response){
                //alert("geetin all allergies" + response);
                vm.allergiesList =response;
                vm.isLoading =false;
            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong in my allergyControl fun'+error.detail + status };
                vm.isLoading =false;
            });
        });

        vm.filterMyAllergie = function()
        {
            vm.isFilterMyAllergie = !vm.isFilterMyAllergie;
        }

        vm.filterByName = function()
        {
            vm.isNameAccending = !vm.isNameAccending;
            vm.allergiesList = $filter("orderBy")(vm.allergiesList, 'name', vm.isNameAccending);
        }
        vm.filterByDate = function()
        {
            vm.isDateAccending = !vm.isDateAccending;
            vm.allergiesList = $filter("orderBy")(vm.allergiesList, 'date', vm.isDateAccending);
        }

        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }

    function MyallergiesoverviewControllerFun($scope, $http, $rootScope, $stateParams, $state, $window, ScrollService, myAllergyOverviewService, $mdDialog ){
        var vm = this;
        vm.isLoading=true;
        vm.isNewMamber = false;
        vm.isNewAllergy = false;
        var d=new Date();
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        vm.myAllergyFormData = {
            patient :null,
            allergyName : null,
            reaction : null,
            date :null,
            note :null,
            today : d.getFullYear()+"-"+(d.getMonth()>9?(d.getMonth()+1):"0"+(d.getMonth()+1))+"-"+(d.getDate()>9?d.getDate():"0"+d.getDate())
        };
        
        vm.oriMyAllergy = angular.copy(vm.myAllergyFormData);


        if($stateParams.myallergyId != "add"){
            myAllergyOverviewService.all($stateParams.myallergyId).success(function(response){
                //alert("getting allergy of the patient");

                vm.myAllergyFormData.patient = response.patient;
                vm.myAllergyFormData.allergyName = response.name;
                vm.myAllergyFormData.reaction = response.reaction;
                vm.myAllergyFormData.date = response.date;
                vm.myAllergyFormData.note = response.note;
                vm.isLoading = false;

            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            });

        }else{
            vm.isLoading = false;
            vm.isNewMamber = true;
        }


        vm.submitAllergy = function(){
            vm.isLoading = true;
            myAllergyOverviewService.save(vm.myAllergyFormData).success(function(){
                vm.isLoading = false;
                $rootScope.$broadcast("MyAllergyUpdateEvent");
                $state.go("dashboard.myallergies");
                $scope.alerts[0]={ type: 'success', msg: vm.myAllergyFormData.allergyName  +' Allergy Successfully Saved'};
            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong '+error.detail + status };
                vm.isLoading = false;
            });
        };


        vm.clearForm = function(){
            vm.myAllergyFormData.allergyName = vm.myAllergyFormData.reaction = vm.myAllergyFormData.date = vm.myAllergyFormData.note = null;
        };


        vm.updateMyAllergy =function(){
            vm.isLoading =true;
            myAllergyOverviewService.update(vm.myAllergyFormData, $stateParams.myallergyId ).success(function(){
                vm.isLoading = false;
                $rootScope.$broadcast("MyAllergyUpdateEvent");
                $state.go("dashboard.myallergies");
                $scope.alerts[0]={ type: 'success', msg: vm.myAllergyFormData.allergyName + ' Successfully Updated'};

            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading = false;
            })
        };



        vm.showConfirm = function(ev) {
            var confirm = $mdDialog.confirm()
              /*.parent(angular.element(document.body))*/
              .title('Would you like to delete your Allergy?')
              .content('all of the records of this allergy will be deleted')
              .ariaLabel('Lucky day')
              .ok('Please do it!')
              .cancel('Please don\'t do it !')
              .targetEvent(ev);

            $mdDialog.show(confirm).then(function() {
                vm.isLoading = true;
                myAllergyOverviewService.remove($stateParams.myallergyId).success(function(){
                    vm.isLoading = false;
                    $rootScope.$broadcast("MyAllergyUpdateEvent");
                    $state.go("dashboard.myallergies");
                    $scope.alerts[0]={ type: 'success', msg: vm.myAllergyFormData.allergyName + ' Successfully Deleted'};
                }).error(function(error, status){
                    $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                    vm.isLoading=false;
                });
            }, function() {
              $scope.alerts[0]={ type: 'danger', msg: vm.myAllergyFormData.allergyName +  ' delete task aborted' };
            });
        };

       /* vm.deleteMyAllergy = function(){
            vm.isLoading = true;
            myAllergyOverviewService.remove($stateParams.myallergyId).success(function(){
                vm.isLoading = false;
                $rootScope.$broadcast("MyAllergyUpdateEvent");
                $state.go("dashboard.myallergies");
                $scope.alerts[0]={ type: 'success', msg: vm.myAllergyFormData.allergyName + ' Successfully Deleted'};

            }).error(function(error, status){
                $scope.alerts[0]={ type: 'danger', msg: 'something gone wrong'+error.detail + status };
                vm.isLoading=false;

            })
        };*/




        vm.closeView=function(){
            $state.go("dashboard.myallergies");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }
})();