/**
 * Created by IDCS12 on 6/4/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.dashboard.controller')
        .controller('DashboardController', dashboardControllerFun);

    dashboardControllerFun.$inject =['$scope', '$http', '$stateParams', '$state', '$window', 'ScrollService'];

    function dashboardControllerFun($scope, $http, $stateParams, $state, $window, ScrollService) {

        var vm = this;
        vm.profile = [
            {
                name : 'My Profile',
                surf : '.myprofile',
                image: 'lib/image/user/128x128.svg'
            },
            {
                name : 'Change Password',
                surf : '.changePass',
                image: 'lib/image/password/128x128.svg'
            }
        ];
        vm.medical = [
            {
                name: 'My Conditions',
                surf: '.myconditions',
                image: 'lib/image/my_conditions/128x128.svg'
            },
            {
                name: 'My Medications',
                surf: '.mymedications',
                image: 'lib/image/my_medications/128x128.svg'
            },
            {
                name: 'My Procedures',
                surf: '.myprocedures',
                image : 'lib/image/my_procedures/128x128.svg'
            },
            {
                name: 'My Allergies',
                surf: '.myallergies',
                image: 'lib/image/my_alergies/128x128.svg'
            }
        ];



        vm.closeView = function () {
            $state.go("home");
            ScrollService.scrollBack();
        };
        ScrollService.scrollBegin();

    }
})();




