/**
 * Created by IDCS12 on 6/4/2015.
 */
(function(){
    'use strict';

    var dashboard_service = angular.module('myApp.dashboard.mymedical.services');
    dashboard_service.service("UpdateMyAllMedicalInformation", updateMyAllMedicalInformation);

    dashboard_service.factory('myConditionService', myConditionServiceFun);
    dashboard_service.factory('myConditionOverviewService', myConditionOverviewServiceFun);

    dashboard_service.factory('myMedicationService', myMedicationServiceFun);
    dashboard_service.factory('myMedicationsOverviewService', myMedicationsOverviewServiceFun);

    dashboard_service.factory('myProcedureService', myProcedureServiceFun);
    dashboard_service.factory('myProcedureOverviewService', myProcedureOverviewServiceFun);

    dashboard_service.factory('MyAllergyService',MyAllergyServiceFun);
    dashboard_service.factory('myAllergyOverviewService', myAllergyOverviewServiceFun);


    updateMyAllMedicalInformation.$inject = ['HttpService', 'CacheService'];

    myConditionServiceFun.$inject = ['HttpService'];
    myConditionOverviewServiceFun.$inject = ['HttpService', '$filter'];

    myMedicationServiceFun.$inject = ['HttpService'];
    myMedicationsOverviewServiceFun.$inject = ['HttpService', '$filter'];

    myProcedureServiceFun.$inject = ['HttpService'];
    myProcedureOverviewServiceFun.$inject = ['HttpService', '$filter'];

    MyAllergyServiceFun.$inject = ['HttpService'];
    myAllergyOverviewServiceFun.$inject = ['HttpService', '$filter'];


    function myConditionServiceFun(HttpService){
        var myConditionApiService = {
            all : myConditionList
        };


        function myConditionList(){
            return(HttpService.PrivateServiceURL('client/patient_condition_list/', 'GET'));
        }

        return myConditionApiService;
    }

    function myConditionOverviewServiceFun(HttpService, $filter){
        var myConditionOverviewApiService = {
            search : searchMyCondition,
            save : saveMyCondition,
            update : updateMyCondition,
            remove : removeMyCondition
        };

        function searchMyCondition(myConditionId){
            return (HttpService.PrivateServiceURL('client/patient_condition_detail/' +myConditionId , 'GET'))
        }

        function saveMyCondition(myConditionFormData){
            var myConditionData = {
                "patient": "",
                "condition": myConditionFormData.condition,
                "date":  $filter('date')(myConditionFormData.date, "yyyy-MM-dd"),
                "doctor": myConditionFormData.doctor,
                "note": myConditionFormData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_condition_list/', myConditionData, 'POST'));
        }

        function updateMyCondition(myConditionData, myCondition_id){
            var data = {
                "id": myCondition_id,
                "patient": myConditionData.patient,
                "condition": myConditionData.condition,
                "date": $filter('date')(myConditionData.date, "yyyy-MM-dd"),
                "doctor": myConditionData.doctor,
                "note": myConditionData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_condition_detail/'+myCondition_id, data, 'PUT'));

        }

        function removeMyCondition(myCondition_id){
            return (HttpService.PrivateServiceURL('client/patient_condition_detail/'+myCondition_id, 'DELETE'));
        }



        return myConditionOverviewApiService
    }


    function myMedicationServiceFun(HttpService){
        var myMedicationApiService = {
            all : myMedicationList
        };

        function myMedicationList(){
            return(HttpService.PrivateServiceURL('client/patient_medication_list/', 'GET'));
        }
        return myMedicationApiService;
    }


    function myMedicationsOverviewServiceFun(HttpService, $filter){
        var myMedicationOverviewApiService = {
            search : searchMyMedication,
            save : saveMyMedication,
            update : updateMyMedication,
            remove : removeMyMedication,
            unit : getUnit,
            method : getMethod
        };

        function getUnit(){
            return (HttpService.PublicServiceURL('client/unit/' , 'GET'));
        }

        function getMethod(){
            return (HttpService.PublicServiceURL('client/method/' , 'GET'));
        }

        function searchMyMedication(myMedicationId){
            return (HttpService.PrivateServiceURL('client/patient_medication_detail/' +myMedicationId , 'GET'))
        }

        function saveMyMedication(myMedicationData){
            var myMedicationData = {
                salt : myMedicationData.salt,
                amount : myMedicationData.amount,
                unit : myMedicationData.unit,
                method : myMedicationData.method,
                date : $filter('date')(myMedicationData.date, "yyyy-MM-dd"),
                note : myMedicationData.note,
                doctor : myMedicationData.doctor,
                frequency : myMedicationData.frequency
            };
            return (HttpService.PostPrivateServiceURL('client/patient_medication_list/', myMedicationData, 'POST'));
        }

        function updateMyMedication(myMedicationData, myMedication_id){
            var data = {
                id : myMedication_id,
                salt : myMedicationData.salt,
                amount : myMedicationData.amount,
                unit : myMedicationData.unit,
                method : myMedicationData.method,
                date : $filter('date')(myMedicationData.date, "yyyy-MM-dd"),
                note : myMedicationData.note,
                doctor : myMedicationData.doctor,
                frequency : myMedicationData.frequency

            };
            return (HttpService.PostPrivateServiceURL('client/patient_medication_detail/'+myMedication_id, data, 'PUT'));

        }

        function removeMyMedication(myMedication_id){
            return (HttpService.PrivateServiceURL('client/patient_medication_detail/'+myMedication_id, 'DELETE'));
        }


        return myMedicationOverviewApiService
    }


    function myProcedureServiceFun(HttpService){
        var myProcedureApiService = {
            all : myProcedureList
        };


        function myProcedureList(){
            return(HttpService.PrivateServiceURL('client/patient_procedure_list/', 'GET'));
        }

        return myProcedureApiService;
    }


    function myProcedureOverviewServiceFun(HttpService, $filter){
        var myProcedureOverviewApiService = {
            search : searchMyProcedure,
            save : saveMyProcedure,
            update : updateMyProcedure,
            remove : removeMyProcedure
        };

        function searchMyProcedure(myProcedureId){
            return (HttpService.PrivateServiceURL('client/patient_procedure_detail/' +myProcedureId , 'GET'))
        }

        function saveMyProcedure(myProcedureFormData){
            var myConditionData = {

                "procedure": myProcedureFormData.procedure,
                "date":  $filter('date')(myProcedureFormData.date, "yyyy-MM-dd"),
                "doctor": myProcedureFormData.doctor,
                "note": myProcedureFormData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_procedure_list/', myConditionData, 'POST'));
        }

        function updateMyProcedure(myProcedureData, myProcedure_id){
            var data = {
                "id": myProcedure_id,
                "patient": "",
                "procedure": myProcedureData.procedure,
                "date": $filter('date')(myProcedureData.date, "yyyy-MM-dd"),
                "doctor": myProcedureData.doctor,
                "note": myProcedureData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_procedure_detail/'+myProcedure_id, data, 'PUT'));

        }

        function removeMyProcedure(myProcedure_id){
            return (HttpService.PrivateServiceURL('client/patient_procedure_detail/'+myProcedure_id, 'DELETE'));
        }
        return myProcedureOverviewApiService;
    }

    function MyAllergyServiceFun(HttpService){
        var AllergyApiService = {
            allAllergy :allAllergy
        };

        function allAllergy(){
            return(HttpService.PrivateServiceURL('client/patient_allergy_list/', 'GET'))
        }
        return  AllergyApiService;
    }




    function myAllergyOverviewServiceFun(HttpService, $filter){
        var myAllergyOverviewApiService = {
            all : allAllergy,
            save : saveMyNewAllergy,
            update :updateMyAllergy,
            remove : removeMyAllergy
        };


        function allAllergy(myallergyId){
            return (HttpService.PrivateServiceURL('client/patient_allergy_detail/' +myallergyId , 'GET'))
        }


        function saveMyNewAllergy(myAllergyFormData){
            var myAllergyData = {
                "id": 2,
                "patient": "Lalit Singh",
                "reaction": myAllergyFormData.reaction,
                "name": myAllergyFormData.allergyName,
                "date":  $filter('date')(myAllergyFormData.date, "yyyy-MM-dd"),
                "note": myAllergyFormData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_allergy_list/', myAllergyData, 'POST'));
        }

        function updateMyAllergy(myAllergyData, myAllergy_id){
            var data = {
                "id": myAllergy_id,
                "patient": myAllergyData.patient,
                "reaction": myAllergyData.reaction,
                "name": myAllergyData.allergyName,
                "date": $filter('date')(myAllergyData.date, "yyyy-MM-dd"),
                "note": myAllergyData.note
            };
            return (HttpService.PostPrivateServiceURL('client/patient_allergy_detail/'+myAllergy_id, data, 'PUT'));


        }

        function removeMyAllergy(myAllergy_id){
            return (HttpService.PrivateServiceURL('client/patient_allergy_detail/'+myAllergy_id, 'DELETE'));
        }

        return myAllergyOverviewApiService;
    }


    //***** Update My Medical Information into Cache*****
    function updateMyAllMedicalInformation(HttpService, CacheService)
    {
        this.allUpdate=function()
        {
            HttpService.PrivateServiceURL('client/patient_condition_list/', 'GET').success(function(response){
                CacheService.setCache("myConditionList", response);
            });
            HttpService.PrivateServiceURL('client/patient_medication_list/', 'GET').success(function(response){
                CacheService.setCache("myMedicationList", response);
            })
            HttpService.PrivateServiceURL('client/patient_procedure_list/', 'GET').success(function(response){
                CacheService.setCache("myProcedureList", response);
            });
        }
    }

})();