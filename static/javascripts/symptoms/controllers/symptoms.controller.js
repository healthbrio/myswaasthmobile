/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    'use strict';

    window.isRefresh=false;
    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController', SymptomsController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsAllController', SymptomsAllController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Rotate_Male', SymptomsController_Rotate_Male);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Gender_Male', SymptomsController_Change_Male);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Rotate_Female', SymptomsController_Rotate_Female);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Gender_Female', SymptomsController_Change_Female);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsAllCauseController', SymptomsAllCauseController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsConditionsController', ConditionsController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsFacilitiesController', SymptomsFacilitiesController);

    angular.module('myApp.symptoms.controller')
        .controller('AvatarController', AvatarController);


    SymptomsController.$inject = ['$scope','$http', 'ScrollService', 'CacheService'];
    SymptomsAllController.$inject = ['$scope','$http', '$interval', '$timeout', '$window', 'symptomsService', 'CacheService','$mdDialog','$rootScope', 'ScrollService'];
    SymptomsController_Rotate_Male.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsController_Rotate_Female.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsController_Change_Male.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService', 'CacheService'];
    SymptomsController_Change_Female.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService', 'CacheService'];
    SymptomsAllCauseController.$inject = ['$scope','$http', '$stateParams', '$state', '$window', 'causesService', 'ScrollService', 'CacheService'];
    ConditionsController.$inject = ['$scope','$rootScope', '$http', '$stateParams', '$state', '$location', '$window', 'ScrollService', 'conditionsService', 'CacheService','$mdDialog'];
    SymptomsFacilitiesController.$inject = ['$scope', '$http', '$stateParams', '$state', 'ScrollService', 'CacheService'];
    AvatarController.$inject = ['$scope','$http', '$stateParams', '$state', '$window','AvatarService', 'ScrollService', 'CacheService', '$mdDialog','$rootScope'];



    function SymptomsController($scope, $http, ScrollService, CacheService){

        var vm = this;
        //outerFun();
        $scope.pageClass="tempclass1";
        ScrollService.scrollBegin();
        setBodyBack(0);

    }
    function SymptomsAllController($scope, $http, $interval, $timeout, $window, symptomsService, CacheService, $mdDialog, $rootScope, ScrollService) {
        var vm = this;

        vm.isAllSymptoms = true;
        vm.symptomsList = [];
        vm.isLife_threatening=false;

        vm.isDisabled=false;

        vm.isLoading=false;
        
        vm.busy = false;

        vm.after = 0;

        vm.countover = true;
        var isloaded=true;
        vm.isFilter=false;


        /*if (CacheService.isCache("symptomsAll_cache")) {
            var data = CacheService.getCache("symptomsAll_cache");

            isloaded=false;
            var i=0;
            vm.isLoading= true;
             $timeout(function(){
                 vm.isLoading=false;

                var interval=$interval(function(){

                vm.symptomsList.push(data.list[i]);
                i++;

                    if(i==data.list.length)
                    {
                        isloaded=true;
                        $interval.cancel(interval);
                    }

                }, 10);
             }, 1500);

            vm.after = data.after;
            window.isRefresh = false;
            vm.busy = false;
        }*/

        vm.loadMore = function () {
            if (CacheService.isCache("symptomsAll_cache")) {

                var data = CacheService.getCache("symptomsAll_cache");

                if(data.after>vm.after)
                {
                    isloaded=false;
                    for(var i=vm.after; i<(vm.after+20); i++)
                    {
                        if(data.list[i]!=null)
                        {
                            vm.symptomsList.push(data.list[i]);
                        }
                    }
                    vm.after+=20;
                    if(data.after<=vm.after)
                    {
                        vm.after = data.after;
                        isloaded=true;
                    }
                    vm.busy = false;
                }
                if(vm.after>data.total)
                {
                    vm.countover=false;
                }
                else
                {
                    vm.countover=true;
                }
                vm.isLoading = false;
            }
            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;
            //var url = "http://52.74.163.60/api/symptoms_list/?offset="+vm.after;
            if (vm.countover) {
                vm.busy = true;
                symptomsService.all(vm.after).success(function (data) {
                    var items = data.results;
                    for (var i = 0; i < items.length; i++) {
                        vm.symptomsList.push(items[i]);
                    }
                    console.log(vm.after);
                    vm.after = vm.after + 20;
                    var data1 = {
                        list: vm.symptomsList,
                        after: vm.after,
                        total : data.count
                    };

                    CacheService.setCache("symptomsAll_cache", data1);
                    vm.busy = false;
                    if (vm.after >= data.count) {
                        console.log("false countover");
                        vm.countover = false;
                    } else {
                        console.log("true countover ");
                    }
                    vm.isLoading = false;
                }.bind(this)).error(function (error) {
                    vm.busy = false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };

                });
            }
        }

        //********************

        var timeout, interval;
        vm.filterListChange=function(filterList)
        {
            
            vm.isFilter=true;
            vm.symptomsList=[];

            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    symptomsService.filterService(filterList).success(function (response) {
                        vm.symptomsList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                        vm.after=(CacheService.getCache("symptomsAll_cache").total+1);
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {
                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("symptomsAll_cache")) {
                var data = CacheService.getCache("symptomsAll_cache");
                isloaded=false;
                var i=0;

                interval=$interval(function(){
                    vm.symptomsList.push(data.list[i]);
                    i++;

                    if(i==20 || i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }

                }, 1);
                vm.after = 20;

                }
            }
        }
        //********************
         var globalcause;
        vm.clickLifeThreatening=function($event, cause)
        {

            //alert(cause.main_symptom.slug);
             globalcause = cause;
            //alert(cause.main_symptom.life_threatening);
            if(cause.main_symptom.life_threatening==true)
            {
                $mdDialog.show({
                  controller: WarningController,
                  templateUrl: $rootScope.static_path + 'templates/symptoms/allsymptoms.dialog1.warning.html',
                  targetEvent: $event
                });
                vm.isLife_threatening=true;
                vm.normalView=cause;
                vm.emergencyView="({stmptoms_id:"+cause.main_symptom.slug+", stmptoms_name:"+cause.main_symptom.name+", msp_enabled:"+cause.main_symptom.msp_enabled+", life_threatening:"+cause.main_symptom.life_threatening+"})";
                $event.preventDefault();
            }
            else
            {
                vm.isLife_threatening=false;
            }
        };
        function WarningController($scope, $mdDialog ){
            $scope.static_path = window.static_path;
            $scope.preUrl = 'allsymptoms';

            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.isLife_threatening=true;
            $scope.normalView=globalcause;
            //alert($scope.normalView.main_symptom.slug);
            //alert(globalcause.main_symptom.slug);
            $scope.emergencyView="({stmptoms_id:"+globalcause.main_symptom.slug+", stmptoms_name:"+globalcause.main_symptom.name+", msp_enabled:"+globalcause.main_symptom.msp_enabled+", life_threatening:"+globalcause.main_symptom.life_threatening+"})";
        }

        //********For Manually Path Copy for Share into Share Button******
        vm.pathCopyForShare=function(isSuccess)
        {
            if(isSuccess)
            {
                $scope.alerts[0]={ type: 'success', msg: 'Data Copy into Clipboard' };
            }
            else
            {
                $scope.alerts[0]={ type: 'danger', msg: 'Browser not support System Clipboard. Copy link Manually' };
            }
        }

        $rootScope.avatar_tab_selected = 1;

        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();

    }

    function SymptomsController_Rotate_Male($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){
        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        vm.flex={
            flexL : 15,
            flexM :65,
            flexR :20
        };
        /*alert(window.screen.availHeight);*/
        if(window.screen.availHeight >=900){
            //alert("greater than 900");
            vm.flex.flexL = 10;
            vm.flex.flexM =80;
            vm.flex.flexR =10;
        }
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){

            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/male_back.jpg",
            isLoading : false
        }
        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/male_back.jpg",
            isLoading : false,
            selectedImgRef :"lib/image/avatar/male_back_hover.jpg",
            hoverImgRef:"lib/image/avatar/male_back_hover.jpg",
            mapArea:[
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'head', full:'head', shape:'poly',
                    coords:'152,90,145,66,139,62,136,43,141,40,150,11,176,1,202,20,205,40,210,38,209,60,195,86,194,90'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'150,90,197,90,217,108,176,113,130,111'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'129,111,104,147,61,160,70,117,106,108'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'217,112,226,132,257,152,282,163,278,126,261,112,236,109'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'back', full:'back', shape:'poly',
                    coords:'127,113,120,134,102,147,103,188,114,211,232,210,243,188,242,165,246,146,229,135,218,111'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'64,164,102,148,104,192,84,269,53,321,23,318,46,244,57,213'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'245,151,243,188,255,234,264,275,283,307,294,329,325,320,305,250,287,209,283,162'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'295,331,325,321,337,338,346,362,334,351,343,381,342,387,336,385,328,358,327,360,331,388,329,389,327,388,318,361,317,361,322,388,319,392,314,383,311,384,302,368'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'24,317.0000030517578,53,326.0000030517578,50,347.0000030517578,38,383.0000030517578,35,383.0000030517578,31,388.0000030517578,25,389.0000030517578,31,361.0000030517578,29,362.0000030517578,21,388.0000030517578,16,389.0000030517578,20,358.0000030517578,20,354.0000030517578,12,383.0000030517578,8,385.0000030517578,4,379.0000030517578,12,352.0000030517578,4,363.0000030517578,1,362.0000030517578,4,347.0000030517578,13,331.0000030517578'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'lower-back', full:'lower-back', shape:'poly',
                    coords:'117,218,229,215,231,279,171,294,118,284'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'buttockrectum', full:'buttockrectum', shape:'poly',
                    coords:'135,300.99999919607615,130,369.99999919607615,168,369.99999919607615,207,363.99999919607615,203,297.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'112,289.99999919607615,149,299.99999919607615,147,355.99999919607615,109,385.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'189,294.99999919607615,229,284.99999919607615,240,370.99999919607615,189,349.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'104,375.99999758822844,167,371.99999758822844,159,477.99999758822844,156,530.9999975882284,157,595.9999975882284,155,641.9999975882284,159,656.9999975882284,133,659.9999975882284,108,566.9999975882284,123,526.9999975882284,120,481.99999758822844'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'178,371.99999758822844,239,375.99999758822844,227,481.99999758822844,227,531.9999975882284,234,568.9999975882284,215,654.9999975882284,189,652.9999975882284,195,597.9999975882284,180,570.9999975882284,192,533.9999975882284,192,458.99999758822844,179,414.99999758822844'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'129,670.9999975882284,160,669.9999975882284,165,691.9999975882284,152,696.9999975882284,130,698.9999975882284,124,697.9999975882284,122,694.9999975882284,131,688.9999975882284'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'188,663.9999975882284,213,663.9999975882284,217,680.9999975882284,224,691.9999975882284,212,698.9999975882284,193,699.9999975882284,183,693.9999975882284,190,681.9999975882284'
                }

            ]
        };
        $scope.name = "rotate";

        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();

    }
    function SymptomsController_Change_Male($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService, CacheService){

        var vm =this;
        $scope.hideImage = false;
        if(CacheService.isCache("avatar_state"))
        {
            if(CacheService.getCache("avatar_state") == 1)
            {
                $scope.hideImageMaleFront = true;
                $scope.hideImageMaleBack = false;
            }
            else if(CacheService.getCache("avatar_state") == 2)
            {
                $scope.hideImageMaleFront = false;
                $scope.hideImageMaleBack = true;
            }
            else
            {
                $scope.hideImageMaleFront = true;
                $scope.hideImageMaleBack = false;
            }
        }
        else
        {
            $scope.hideImageMaleFront = true;
            $scope.hideImageMaleBack = false;
        }
        vm.flex={
            flexL : 15,
            flexM :65,
            flexR :20
        };
        /*alert(window.screen.availHeight);*/
        vm.windowHeight = $window.innerHeight;
        vm.viewheight = {
            'height': (vm.windowHeight - 171) + "px"
        };
        //alert(window.screen.availHeight);
        if(window.screen.availHeight >=900){
            //alert("greater than 900");
            vm.flex.flexL = 10;
            vm.flex.flexM =80;
            vm.flex.flexR =10;
        }

        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }
        //alert(vm.isSlide);
        vm.rotate = function (){
            $scope.hideImage = true;

            if($scope.hideImageMaleBack == false)
            {
                CacheService.setCache("avatar_state", 2);
                $timeout(function(){
                    $scope.hideImageMaleBack = true;
                    $scope.hideImageMaleFront = false;
                    $scope.hideImage = false;
                }, 500);
            }
            else
            {
                CacheService.setCache("avatar_state", 1);
                $timeout(function(){
                    $scope.hideImageMaleBack = false;
                    $scope.hideImageMaleFront = true;
                    $scope.hideImage = false;
                }, 500);
            }

        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo=[{
            src : $scope.static_path+"lib/image/avatar/male_front.jpg",
            isLoading : true,
            selectedImgRef :"lib/image/avatar/male_front_hover.jpg",
            hoverImgRef:"lib/image/avatar/male_front_hover.jpg",
            mapArea:[
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'head', full:'head', shape:'poly',
                    coords:'173,92,157,88,150,80,144,65,139,64,135,42,136,39,142,42,146,16,155,5,171,1,181,1,194,8,201,20,205,40,210,39,212,46,208,63,202,65,199,74,195,81,186,88,183,90'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'138,74,160,82,180,72,182,88,201,100,159,110,117,101,132,94,140,84'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'117,101,89,118,75,143,58,164,62,117,74,106,92,101'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'202,100,232,117,244,142,260,162,261,138,255,114,239,103'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'chest', full:'chest', shape:'poly',
                    coords:'113,110,103,162,133,192,175,187,223,185,241,152,223,112,218,109,185,123,158,120,138,119'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'103,150,103,189,83,266,54,321,25,318,42,248,59,205,63,168,87,146'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'240,149,242,186,261,265,287,290,292,329,322,321,303,248,289,215,279,157,234,135'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'24,318,53,326,47,362,41,381,35,383,31,388,27,390,25,386,29,362,23,385,20,389,15,388,16,375,19,360,15,372,10,384,6,385,5,380,13,351,2,365,0,361,8,341,17,328,21,324'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'294,326,324,319,339,339,346,361,345,365,334,348,343,382,341,385,336,382,328,358,328,367,331,387,328,389,325,388,318,361,321,387,319,388,316,388,309,363,313,382,310,384,303,375,298,351,295,336'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'abdomen', full:'abdomen', shape:'poly',
                    coords:'102,167,135,194,177,197,220,188,241,165,240,185,233,203,228,230,229,269,230,296,201,314,176,326,159,327,136,314,120,305,114,285,123,253,117,213,104,187'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'115,311,134,320,130,380,106,373,107,324'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'203,321,230,306,239,391,207,390'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'pelvis', full:'pelvis', shape:'poly',
                    coords:'136,314,132,382,168,380,201,378,201,319,171,326'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'102,381,172,385,153,527,154,632,156,654,133,650,111,571,127,524'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'177,380,243,377,228,481,228,544,234,579,212,654,189,654,193,606,187,572,193,541,191,479'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'130,663,156,662,161,697,123,696'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'185,658,210,659,224,695,207,700,188,696'
                }
            ]

        },
        {
            src : $scope.static_path+"lib/image/avatar/male_back.jpg",
            isLoading : true,
            selectedImgRef :"lib/image/avatar/male_back_hover.jpg",
            hoverImgRef:"lib/image/avatar/male_back_hover.jpg",
            mapArea:[
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'head', full:'head', shape:'poly',
                    coords:'152,90,145,66,139,62,136,43,141,40,150,11,176,1,202,20,205,40,210,38,209,60,195,86,194,90'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'150,90,197,90,217,108,176,113,130,111'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'129,111,104,147,61,160,70,117,106,108'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'217,112,226,132,257,152,282,163,278,126,261,112,236,109'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'back', full:'back', shape:'poly',
                    coords:'127,113,120,134,102,147,103,188,114,211,232,210,243,188,242,165,246,146,229,135,218,111'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'64,164,102,148,104,192,84,269,53,321,23,318,46,244,57,213'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'245,151,243,188,255,234,264,275,283,307,294,329,325,320,305,250,287,209,283,162'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'295,331,325,321,337,338,346,362,334,351,343,381,342,387,336,385,328,358,327,360,331,388,329,389,327,388,318,361,317,361,322,388,319,392,314,383,311,384,302,368'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'24,317.0000030517578,53,326.0000030517578,50,347.0000030517578,38,383.0000030517578,35,383.0000030517578,31,388.0000030517578,25,389.0000030517578,31,361.0000030517578,29,362.0000030517578,21,388.0000030517578,16,389.0000030517578,20,358.0000030517578,20,354.0000030517578,12,383.0000030517578,8,385.0000030517578,4,379.0000030517578,12,352.0000030517578,4,363.0000030517578,1,362.0000030517578,4,347.0000030517578,13,331.0000030517578'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'lower-back', full:'lower-back', shape:'poly',
                    coords:'117,218,229,215,231,279,171,294,118,284'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'buttockrectum', full:'buttockrectum', shape:'poly',
                    coords:'135,300.99999919607615,130,369.99999919607615,168,369.99999919607615,207,363.99999919607615,203,297.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'112,289.99999919607615,149,299.99999919607615,147,355.99999919607615,109,385.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'189,294.99999919607615,229,284.99999919607615,240,370.99999919607615,189,349.99999919607615'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'104,375.99999758822844,167,371.99999758822844,159,477.99999758822844,156,530.9999975882284,157,595.9999975882284,155,641.9999975882284,159,656.9999975882284,133,659.9999975882284,108,566.9999975882284,123,526.9999975882284,120,481.99999758822844'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'178,371.99999758822844,239,375.99999758822844,227,481.99999758822844,227,531.9999975882284,234,568.9999975882284,215,654.9999975882284,189,652.9999975882284,195,597.9999975882284,180,570.9999975882284,192,533.9999975882284,192,458.99999758822844,179,414.99999758822844'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'129,670.9999975882284,160,669.9999975882284,165,691.9999975882284,152,696.9999975882284,130,698.9999975882284,124,697.9999975882284,122,694.9999975882284,131,688.9999975882284'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'188,663.9999975882284,213,663.9999975882284,217,680.9999975882284,224,691.9999975882284,212,698.9999975882284,193,699.9999975882284,183,693.9999975882284,190,681.9999975882284'
                }

            ]
        }];

        $scope.name="ajay";

        if(!CacheService.isCache("avatar_state"))
        {
            CacheService.setCache("avatar_state", 1);
        }
        $rootScope.clickOnTabs(false);
        $rootScope.avatar_tab_selected = 0;
        ScrollService.scrollBegin();
    }
    function SymptomsController_Rotate_Female($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){
        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.viewheight = {
            'height': (vm.windowHeight - 171) + "px"
        };
         vm.flex={
            flexL : 15,
            flexM :65,
            flexR :20
        };
        if(window.screen.availHeight >=900){
            //alert("greater than 900");
            vm.flex.flexL = 10;
            vm.flex.flexM =80;
            vm.flex.flexR =10;
        }
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/female_back.jpg",
            isLoading : false,
            selectedImgRef :"lib/image/avatar/female_back_selected.jpg",
            hoverImgRef:"lib/image/avatar/female_back_selected.jpg",
            mapArea:[
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'head', full:'head', shape:'poly',
                    coords:'171,100,151,89,148,83,144,80,137,69,138,60,141,58,143,34,150,19,156,14,159,6,173,0,183,3,190,11,198,19,203,31,206,61,208,60,207,71,202,81,196,86,187,97,180,98'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'151,94,154,113,151,120,140,127,129,129,163,133,183,134,205,134,219,130,201,123,192,115,195,99,196,94,180,99,173,101,165,97'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'122,132,100,167,70,173,78,141,100,130'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'218,132,248,161,275,167,268,142,257,133,244,131'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'back', full:'back', shape:'poly',
                    coords:'127,132,105,176,127,236,219,237,244,173,226,131'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'68,178,98,168,100,176,85,224,77,286,60,344,38,337,46,310,60,231,68,193'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'248,162,244,174,263,223,268,278,285,334,304,334,296,267,288,232,276,167'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'38,344,60,346,61,371,60,382.00001017252606,55,393.00001017252606,56,377.00001017252606,54,376.00001017252606,51,398.00001017252606,48,397.00001017252606,49,379.00001017252606,49,378.00001017252606,48,377.00001017252606,45,398.00001017252606,43,400.00001017252606,42,377.00001017252606,41,376.00001017252606,37,397.00001017252606,35,399.00001017252606,35,366.00001017252606,31,377.00001017252606,27,381.00001017252606,25,378.00001017252606,30,357.00001017252606'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'287,340,306,337,316,356,321,378,319,380,311,365,312,398,311,399,306,377,304,377,305,400,303,401,299,378,297,378,299,398,296,397,291,375,292,390,290,390,285,374'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'lower-back', full:'lower-back', shape:'poly',
                    coords:'127,239,220,240,214,264,226,307,116,312,128,280,132,265'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'buttockrectum', full:'buttockrectum', shape:'poly',
                    coords:'143,320.00000508626306,198,316.00000508626306,206,396.00000508626306,149,394.00000508626306,143,352.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'115,321.00000508626306,144,318.00000508626306,142,390.00000508626306,140,408.00000508626306,106,405.00000508626306,106,360.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'225,314.00000508626306,194,316.00000508626306,194,404.00000508626306,239,406.00000508626306,242,372.00000508626306,233,332.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'105,416,168,410,158,518,152,542,152,590,146,619,143,676,124,672,119,626,108,574,119,534,120,486'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'177,412,240,400,226,498,232,545,236,582,220,668,201,668,203,612,192,578,196,542,190,486,186,468'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'124,676,144,677,142,698,125,698,110,696,112,693,121,686'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'203,672,222,672,225,686,236,691,233,696,223,698,208,697,204,690'
                }
            ]
        };

        $scope.name= "female Back";


        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();
    }
    function SymptomsController_Change_Female($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService, CacheService){
        var vm =this;
        $scope.hideImage = false;

        if(CacheService.isCache("avatar_state"))
        {
            if(CacheService.getCache("avatar_state") == 3)
            {
                $scope.hideImageFemaleFront = true;
                $scope.hideImageFemaleBack = false;
            }
            else if(CacheService.getCache("avatar_state") == 4)
            {
                $scope.hideImageFemaleFront = false;
                $scope.hideImageFemaleBack = true;
            }
            else
            {
                $scope.hideImageFemaleFront = true;
                $scope.hideImageFemaleBack = false;
            }
        }
        else
        {
            $scope.hideImageFemaleFront = true;
            $scope.hideImageFemaleBack = false;
        }

        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;
        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        vm.flex={
            flexL : 15,
            flexM :65,
            flexR :20
        };
        if(window.screen.availHeight >=900){
            //alert("greater than 900");
            vm.flex.flexL = 10;
            vm.flex.flexM =80;
            vm.flex.flexR =10;
        }


        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            $scope.hideImage = true;

            /*$rootScope.avataranim ="rotate";
            $scope.hideImageMaleFront = true;*/
            if($scope.hideImageFemaleBack == false)
            {
                CacheService.setCache("avatar_state", 4);
                $timeout(function(){
                    $scope.hideImageFemaleBack = true;
                    $scope.hideImageFemaleFront = false;
                    $scope.hideImage = false;
                }, 500);
            }
            else
            {
                CacheService.setCache("avatar_state", 3);
                $timeout(function(){
                    $scope.hideImageFemaleBack = false;
                    $scope.hideImageFemaleFront = true;
                    $scope.hideImage = false;
                }, 500);
            }
        }

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo=[{
            src : $scope.static_path+"lib/image/avatar/female_front.jpg",
            isLoading : true,
            selectedImgRef :"lib/image/avatar/female_front_selected.jpg",
            hoverImgRef:"lib/image/avatar/female_front_selected.jpg",
            mapArea:[
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'head', full:'head', shape:'poly',
                    coords:'172,103,150,95,147,88,147,79,140,74,138,62,141,58,143,28,156,13,157,8,166,0,175,0,188,5,189,11,202,25,205,45,205,59,207,64,205,75,199,79,199,88,191,99,183,101'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'152,94,154,111,151,118,142,124,125,128,157,132,177,132,216,128,198,120,192,113,194,95,187,100,175,101,164,100,159,99'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'127,129,100,164,68,164,75,142,89,132'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'223,130,250,162,278,168,272,144,256,135'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'chest', full:'chest', shape:'poly',
                    coords:'223,131,244,170,228,212,224,221,121,223,102,172,128,130,172,140,194,141'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'68,169,98,164,83,225,76,290,60,338,40,333,57,243,64,199'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'243,170,277,173,289,251,305,330,281,340,264,230'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'37,341,59,344,60,375.00002034505206,55,391.00002034505206,53,392.00002034505206,54,375.00002034505206,51,395.00002034505206,47,397.00002034505206,49,378.00002034505206,47,377.00002034505206,44,399.00002034505206,42,399.00002034505206,42,378.00002034505206,40,376.00002034505206,37,397.00002034505206,34,398.00002034505206,35,366.00002034505206,29,378.00002034505206,26,380.00002034505206,30,360.00002034505206'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'287,341,305,332,312,352,321,378,319,380,313,365,312,365,313,394,312,398,310,399,306,376,304,376,305,399,303,399,298,378,297,377,298,397,296,398,291,375,293,392,290,391,285,373'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'abdomen', full:'abdomen', shape:'poly',
                    coords:'124,229,224,225,213,267,231,320,232,333,114,336,120,295,132,264'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'108,344,145,342,144,400,105,400,104,372'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'200,341,201,397,241,400,241,367,231,332'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'pelvis', full:'pelvis', shape:'poly',
                    coords:'142,341,140,395,204,396,202,339'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'104,402,168,403,153,531,152,579,144,633,144,659,124,662,109,578,119,528,115,473'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'179,400,244,400,228,506,236,563,224,648,220,664,199,660,204,624,192,582,195,542,188,508'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'124,664,144,665,142,692,137,697,118,696,113,692,121,687'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'200,669,221,668,236,696,212,699,204,696'
                }
            ]
        },
        {
            src : $scope.static_path+"lib/image/avatar/female_back.jpg",
            isLoading : true,
            selectedImgRef :"lib/image/avatar/female_back_selected.jpg",
            hoverImgRef:"lib/image/avatar/female_back_selected.jpg",
            mapArea:[
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'head', full:'head', shape:'poly',
                    coords:'171,100,151,89,148,83,144,80,137,69,138,60,141,58,143,34,150,19,156,14,159,6,173,0,183,3,190,11,198,19,203,31,206,61,208,60,207,71,202,81,196,86,187,97,180,98'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'151,94,154,113,151,120,140,127,129,129,163,133,183,134,205,134,219,130,201,123,192,115,195,99,196,94,180,99,173,101,165,97'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'122,132,100,167,70,173,78,141,100,130'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'218,132,248,161,275,167,268,142,257,133,244,131'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'back', full:'back', shape:'poly',
                    coords:'127,132,105,176,127,236,219,237,244,173,226,131'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'68,178,98,168,100,176,85,224,77,286,60,344,38,337,46,310,60,231,68,193'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'248,162,244,174,263,223,268,278,285,334,304,334,296,267,288,232,276,167'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'38,344,60,346,61,371,60,382.00001017252606,55,393.00001017252606,56,377.00001017252606,54,376.00001017252606,51,398.00001017252606,48,397.00001017252606,49,379.00001017252606,49,378.00001017252606,48,377.00001017252606,45,398.00001017252606,43,400.00001017252606,42,377.00001017252606,41,376.00001017252606,37,397.00001017252606,35,399.00001017252606,35,366.00001017252606,31,377.00001017252606,27,381.00001017252606,25,378.00001017252606,30,357.00001017252606'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'287,340,306,337,316,356,321,378,319,380,311,365,312,398,311,399,306,377,304,377,305,400,303,401,299,378,297,378,299,398,296,397,291,375,292,390,290,390,285,374'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'lower-back', full:'lower-back', shape:'poly',
                    coords:'127,239,220,240,214,264,226,307,116,312,128,280,132,265'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'buttockrectum', full:'buttockrectum', shape:'poly',
                    coords:'143,320.00000508626306,198,316.00000508626306,206,396.00000508626306,149,394.00000508626306,143,352.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'115,321.00000508626306,144,318.00000508626306,142,390.00000508626306,140,408.00000508626306,106,405.00000508626306,106,360.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'225,314.00000508626306,194,316.00000508626306,194,404.00000508626306,239,406.00000508626306,242,372.00000508626306,233,332.00000508626306'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'105,416,168,410,158,518,152,542,152,590,146,619,143,676,124,672,119,626,108,574,119,534,120,486'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'177,412,240,400,226,498,232,545,236,582,220,668,201,668,203,612,192,578,196,542,190,486,186,468'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'124,676,144,677,142,698,125,698,110,696,112,693,121,686'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'203,672,222,672,225,686,236,691,233,696,223,698,208,697,204,690'
                }
            ]
        }];
        $scope.name="female Front";
        if(!CacheService.isCache("avatar_state"))
        {
            CacheService.setCache("avatar_state", 3);
        }
        $rootScope.clickOnTabs(false);
        $rootScope.avatar_tab_selected = 0;
        ScrollService.scrollBegin();
    }


    function SymptomsAllCauseController($scope, $http, $stateParams, $state, $window, causesService, ScrollService, CacheService)
    {
        var vm = this;
        vm.title=$stateParams.stmptoms_name;

        vm.causesList = [];
        vm.isLoading=true;
        $scope.filter_by_gender=true;
        $scope.filter_by_age=2;
        vm.filter_by_conditions = [];
        var filter="";
        vm.age="6-17";
        vm.isNotCause=false;
        var ismsp_enabled=$stateParams.msp_enabled;
        var new_symptom_slug = "";

        //alert($stateParams.symptoms_id);



        /*causesService.filter($stateParams.symptoms_id).success(function(response){
            console.log(JSON.stringify(response));
        }).error(function(error, status){
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN ' + error  + " " + status};
        });*/

        //vm.isLife_threatening=false;

        /*if($stateParams.life_threatening==true)
        {
            vm.isLife_threatening=$stateParams.life_threatening;
        }*/

        if(CacheService.isCache("myAge"))
        {
            $scope.filter_by_age=CacheService.getCache("myAge");
            if($scope.filter_by_age==1)
            {
                vm.age="0-5";
            }
            else if($scope.filter_by_age==2)
            {
                vm.age="6-17";
            }
            else if($scope.filter_by_age==3)
            {
                vm.age="18-59";
            }
            else if($scope.filter_by_age==4)
            {
                vm.age="60+";
            }
        }
        if(CacheService.isCache("myGender"))
        {
            $scope.filter_by_gender=CacheService.getCache("myGender");
        }
        filter=$stateParams.symptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age+"/";

        if(ismsp_enabled==null)
        {
            if(CacheService.isCache("symptoms_"+filter))
            {
                ismsp_enabled=CacheService.getCache("symptoms_"+filter)[2].msp_enabled[0].is_active;
            }
            else
            {
                ismsp_enabled=false;
            }

        }

        if(ismsp_enabled==true)
        {
            if(CacheService.isCache("symptoms_"+filter))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+filter);
                ismsp_enabled=vm.causesList[2].msp_enabled[0].is_active;
                //vm.title=vm.causesList.name;
                vm.isLoading=false;
            }
        }
        else
        {
            ismsp_enabled=false;
            if(CacheService.isCache("symptoms_"+$stateParams.symptoms_id))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+$stateParams.symptoms_id);
                //ismsp_enabled=vm.causesList.msp_enabled;
                //vm.title=vm.causesList.name;
                vm.title=vm.causesList[1].msp_enabled[0].symptom;
                vm.isLoading=false;

            }
            else
            {
                causesService.all($stateParams.symptoms_id).success(function(response){
                    vm.causesList = response;
                    vm.title=response[1].msp_enabled[0].symptom;
                    CacheService.setCache("symptoms_"+$stateParams.symptoms_id, response);
                    vm.isLoading=false;
                  }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        $scope.$watch("filter_by_age", function(newValue, oldValue){

            if(oldValue!=newValue)
            {
                CacheService.setCache("myAge", $scope.filter_by_age);
                if($scope.filter_by_age==1)
                {
                    vm.age="0-5";
                }
                else if($scope.filter_by_age==2)
                {
                    vm.age="6-17";
                }
                else if($scope.filter_by_age==3)
                {
                    vm.age="18-59";
                }
                else if($scope.filter_by_age==4)
                {
                    vm.age="60+";
                }

                filter=$stateParams.symptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age+"/";
                if(ismsp_enabled)
                {
                    getFilter(filter);
                }
            }
        });
        $scope.$watch("filter_by_gender", function(){

            CacheService.setCache("myGender", $scope.filter_by_gender);
            filter=$stateParams.symptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age+"/";
            if(ismsp_enabled)
            {
                getFilter(filter);
            }
        });

        vm.clickRelatedSymptoms = function($event, new_symptom){

            new_symptom_slug="";
            angular.forEach(new_symptom, function(value, key){
                if(value.is_msp_active==true)
                {
                    new_symptom_slug+=value.slug+"/";
                }
            });
            filter=$stateParams.symptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age+"/"+new_symptom_slug;
            getFilter(filter);
        }

        function getFilter(filter)
        {
            vm.isNotCause=false;
            //vm.causesList=[];
            if(CacheService.isCache("symptoms_"+filter))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+filter);
                if(vm.causesList.conditions)
                {
                    vm.isNotCause=true;
                }
                
                vm.title = vm.causesList[2].msp_enabled[0].symptom;
                ismsp_enabled=vm.causesList[2].msp_enabled[0].is_active;
                //vm.title=vm.causesList.name;
                vm.isLoading=false;
            }
            else
            {
                vm.isLoading=true;
                causesService.all(filter).success(function(response){

                    vm.isLoading=false;
                    vm.causesList = [];
                    
                    if(response[0].conditions.length===0)
                    {
                        response = [{
                            conditions : []
                        },
                        {
                            msp_symptoms : []
                        },
                        {
                            msp_enabled : [{
                                is_active: true,
                                symptom : $stateParams.stmptoms_name
                            }]
                        }];
                        vm.causesList = response;
                        vm.isLoading=false;
                        CacheService.setCache("symptoms_"+filter, response);
                    }
                    else
                    {
                        if(!response[0].conditions)
                        {
                            vm.isNotCause=true;
                        }
                        response.msp_enabled=true;
                        vm.causesList = response;

                        CacheService.setCache("symptoms_"+filter, response);
                    }
                    vm.title = response[2].msp_enabled[0].symptom;
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".causes")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();

    }

    function ConditionsController($scope, $rootScope, $http, $stateParams, $state, $location, $window, ScrollService, conditionsService, CacheService , $mdDialog)
    {
        var vm = this;
        $scope.condition_overview_menu=null;
        vm.isOverviewTab=false;
        vm.isFurtherReadingTab=false;
        vm.isImagesVideosTab=false;
        vm.isHealthArticle=false;

        vm.isImageViewerShow=false;
        vm.isVideoViewerShow=false;
        vm.imageViewerPath="";
        vm.imageViewerTitle="";
        vm.isArticleViewerShow=false;
        vm.articleViewerTitle="";
        vm.articleViewerDesc="";

        vm.conditionsOveriew=[];
        vm.isLoading=true;

        vm.myConditionId=0;
        vm.isStarFill=false;

        vm.viewheight = {
            height : ($window.innerHeight-171)+"px"
        }

        $scope.whereCouldGoData = [];

        if(CacheService.isCache("conditions_"+$stateParams.cause_id))
        {
            //***Search sessionStorage when click any Cause
            vm.conditionsOveriew=CacheService.getCache("conditions_"+$stateParams.cause_id);
            vm.title=vm.conditionsOveriew[0].name;
            vm.isLoading=false;
            //********* For Where Could Go Panel**********
            $scope.whereCouldGoData = [{
                    condition_slug : vm.conditionsOveriew[0].slug
                },
                {
                    specialties : vm.conditionsOveriew[0].medical_specialties
                },
                {
                    medications : vm.conditionsOveriew[0].medications
                },
                {
                    facility : vm.conditionsOveriew[0].medical_facility_categories
                }];
            //************************
        }
        else
        {
            conditionsService.all($stateParams.cause_id).success(function(response){
                    vm.conditionsOveriew = response;
                    vm.title=vm.conditionsOveriew[0].name;
                    CacheService.setCache("conditions_"+$stateParams.cause_id+"", vm.conditionsOveriew);
                    vm.isLoading=false;
                    //********* For Where Could Go Panel**********
                    $scope.whereCouldGoData = [{
                        condition_slug : vm.conditionsOveriew[0].slug
                    },
                    {
                        specialties : vm.conditionsOveriew[0].medical_specialties
                    },
                    {
                        medications : vm.conditionsOveriew[0].medications
                    },
                    {
                        facility : vm.conditionsOveriew[0].medical_facility_categories
                    }];
                    //************************
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }


        $scope.$watch("condition_overview_menu", function(){

            if($scope.condition_overview_menu==null)
            {
                vm.isOverviewTab=true;
                vm.isFurtherReadingTab=false;
                vm.isImagesVideosTab=false;
                vm.isHealthArticle=false;
            }
            if($scope.condition_overview_menu==1)
            {
                //$state.go("conditions.symptomsConditions");
                vm.isOverviewTab=true;
                vm.isFurtherReadingTab=false;
                vm.isImagesVideosTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==2)
            {
                //$state.go("conditions.symptomsConditions.rurther_reading");
                vm.isFurtherReadingTab=true;
                vm.isOverviewTab=false;
                vm.isImagesVideosTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==3)
            {
                //$state.go("conditions.symptomsConditions.images_videos");
                vm.isImagesVideosTab=true;
                vm.isOverviewTab=false;
                vm.isFurtherReadingTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==4)
            {
                //$state.go("conditions.symptomsConditions.article");
                vm.isHealthArticle=true;
                vm.isOverviewTab=false;
                vm.isFurtherReadingTab=false;
                vm.isImagesVideosTab=false;
                ScrollService.scrollTop(".scroll_top");
            }


        });

        vm.showImage = function(ev, index) {

            vm.index=index;
            $mdDialog.show({
              controller: DialogController,
              templateUrl: $rootScope.static_path +'templates/imgvideodialog/dialog1.img.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
          };

        function DialogController($scope, $mdDialog) {
           // Set of Photos

            $scope.static_path = window.static_path;
            $scope.photos = [];
            for(var n=0; n<vm.conditionsOveriew[0].images.length; n++)
            {
                $scope.photos.push({src: vm.conditionsOveriew[0].images[n].desktop_url, desc: vm.conditionsOveriew[0].images[n].title, isLoading:false});
            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };

        }

        vm.showVideo = function(ev, index) {
            vm.index=index;
            $mdDialog.show({
              controller: VideoController,
              templateUrl: $rootScope.static_path+ 'templates/imgvideodialog/dialog1.video.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
        };

        function VideoController($scope, $mdDialog) {
           // Set of Photos
            $scope.static_path = window.static_path;
            $scope.videos = [];

            for(var n=0; n<vm.conditionsOveriew[0].videos.length; n++)
            {
                $scope.videos.push({src: vm.conditionsOveriew[0].videos[n].player_links, desc: vm.conditionsOveriew[0].videos[n].title});
                /*$scope.videosthumbs.push({src: vm.proceduresOveriew[0].video[n].thumbnail_url, desc: vm.proceduresOveriew[0].video[n].title});*/
            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                alert("prev");
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.videos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                alert("next");
                $scope._Index = ($scope._Index < $scope.videos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };

        }


        vm.showArticles = function(ev, index) {
            vm.index=index;
            $mdDialog.show({
              controller: articleController,
              templateUrl: $rootScope.static_path+ 'templates/imgvideodialog/dialog1.articles.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
        };

        function articleController($scope, $mdDialog){
            // Set of Photos
            vm.isArticleViewerShow=true;
            /*vm.articleViewerTitle=article.title;
            vm.articleViewerDesc=article.article_body;*/
            $scope.static_path = window.static_path;
            $scope.articles = [];
            for(var n=0; n<vm.conditionsOveriew[0].article.length; n++)
            {
                $scope.articles.push({src: vm.conditionsOveriew[0].article[n].article_body, slug : vm.conditionsOveriew[0].article[n].slug, desc:vm.conditionsOveriew[0].article[n].title});

            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                alert("prev");
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.articles.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                alert("next");
                $scope._Index = ($scope._Index < $scope.articles.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };
        }

        //****** Check Condition already fill or not*******
        if(CacheService.isCache("myConditionList"))
        {
            var myConditionList=CacheService.getCache("myConditionList");
            for(var m=0; m<myConditionList.length; m++)
            {
                if(myConditionList[m].condition==vm.title)
                {
                    vm.isStarFill=true;
                    vm.myConditionId=myConditionList[m].id;
                }
            }
        }

        vm.closeView=function(){

            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".conditions_overview")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    }

    function SymptomsFacilitiesController($scope, $http, $stateParams, $state, ScrollService, CacheService)
    {
        var vm = this;
        vm.title=$stateParams.title_name2;
        vm.isLoading=true;
        /*vm.title=$stateParams.title_name1;
        vm.conditionList=[];
        conditionsService.all($stateParams).success(function(response){
            vm.conditionList = response;
            }).error(function(){
                alert("error")
            });*/

        vm.closeView=function(){

            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".symptomsFacilities")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    }

    function AvatarController($scope, $http, $stateParams, $state, $window, AvatarService, ScrollService, CacheService, $mdDialog ,$rootScope)
    {
        var vms=this;
        vms.isLoading=true;
        vms.title=$stateParams.avatar_id;
        vms.rotateURL="";
        //AvatarService.all($stateParams);
        vms.causesList = [];
        vms.isLife_threatening=false;
        vms.windowHeight = $window.innerHeight;
        vms.windowHeight = vms.windowHeight - 171;

        vms.viewheight = {
            'height': vms.windowHeight + "px"
        };
        var cacheKey=$stateParams.avatar_code+"/"+$stateParams.avatar_id;

        var avatar_url_="";
        if($stateParams.avatar_code == 1 || $stateParams.avatar_code == 2)
        {
            avatar_url_="api/symptoms_list/male/"+$stateParams.avatar_id+"/";
            CacheService.setCache("myGender", false);
        }
        else if($stateParams.avatar_code == 3 || $stateParams.avatar_code == 4)
        {
            avatar_url_="api/symptoms_list/female/"+$stateParams.avatar_id+"/";
            CacheService.setCache("myGender", true);
        }

        if(CacheService.isCache(cacheKey))
        {
            //alert("from cache");
            //***Search sessionStorage when click any Cause
            vms.causesList=CacheService.getCache(cacheKey);
            vms.isLoading=false;
        }
        else
        {
            //alert("from api");
            //***get Data from API and store it sessionStorage
            AvatarService.all(avatar_url_).success(function(response){
                    vms.causesList = response;
                    CacheService.setCache(cacheKey, vms.causesList);
                    vms.isLoading=false;
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }

        var globalcause;
        vms.clickLifeThreatening=function($event, cause)
        {

            globalcause = cause;
            if(cause.life_threatening==true)
            {
                $mdDialog.show({
                  controller: WarningController,
                  templateUrl: $rootScope.static_path + 'templates/symptoms/dialog1.warning.html',
                  targetEvent: $event
                });
                vms.isLife_threatening=true;
                vms.normalView=cause;
                vms.emergencyView="({stmptoms_id:"+cause.slug+", stmptoms_name:"+cause.name+", msp_enabled:"+cause.msp_enabled+", life_threatening:"+cause.life_threatening+"})";
                $event.preventDefault();
            }
            else
            {
                vms.isLife_threatening=false;
            }
        };
        function WarningController($scope, $mdDialog ){
           $scope.static_path = window.static_path;
           // alert($stateParams.avatar_code);
            if($stateParams.avatar_code==1)
            {
                $scope.preUrl = 'male_front';
            }
            if($stateParams.avatar_code==2)
            {
                $scope.preUrl = 'male_back';
            }
            if($stateParams.avatar_code==3)
            {
                $scope.preUrl = 'female_front';
            }
            if($stateParams.avatar_code==4)
            {
                $scope.preUrl = 'female_back';
            }

            $scope.hide = function() {
                $mdDialog.hide();
              };
            $scope.isLife_threatening=true;
            $scope.normalView=globalcause;
            
            $scope.emergencyView="({stmptoms_id:"+globalcause.slug+", stmptoms_name:"+globalcause.name+", msp_enabled:"+globalcause.msp_enabled+", life_threatening:"+globalcause.life_threatening+"})";
        }


        vms.closeView=function(){
            if($stateParams.avatar_code==0)
            {
                $state.go("symptoms");
            }
            if($stateParams.avatar_code==1)
            {
                $state.go("symptoms/male");
            }
            if($stateParams.avatar_code==2)
            {
                $state.go("symptoms/male_back");
            }
            if($stateParams.avatar_code==3)
            {
                $state.go("symptoms/female");
            }
            if($stateParams.avatar_code==4)
            {
                $state.go("symptoms/female_back");
            }
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    };
})();



function outerFun()
{
    $('#avatar_img').mapster({
    mapKey: 'avatar-key',
    singleSelect: true,
    onClick:function(e){
        //window.location=$(this).attr("data-href");
        }
    });


/*    $(".mytemp").mCustomScrollbar({
          axis:"x",
          theme:"dark-thin",
          autoExpandScrollbar:true,
          advanced:{autoExpandHorizontalScroll:true}
        });*/

}
