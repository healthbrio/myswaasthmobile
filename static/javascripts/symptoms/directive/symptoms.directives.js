/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';


    angular.module('myApp.symptoms.directives')
        .directive('frontRotate', frameRotateFun);


    frameRotateFun.$inject = ['$state', '$rootScope', '$location', '$timeout', 'CacheService'];




    function frameRotateFun($state, $rootScope, $location, $timeout, CacheService){
         return{
            restrict : 'AE',

            link : function (scope, element, attrs){
               var rotateFront = document.getElementById(attrs.buttonid);
                //alert(attrs.buttonid);
                    rotateFront.onclick = function(){

                        if(CacheService.isCache("avatar_state"))
                        {
                            if(CacheService.getCache("avatar_state") == 1)
                            {
                                attrs.buttonid = "rotateBack";
                            }
                            if(CacheService.getCache("avatar_state") == 2)
                            {
                                attrs.buttonid = "rotateFront";
                            }
                            if(CacheService.getCache("avatar_state") == 3)
                            {
                                attrs.buttonid = "rotateFemaleBack";
                            }
                            if(CacheService.getCache("avatar_state") == 4)
                            {
                                attrs.buttonid = "rotateFemaleFront";
                            }
                        }

                        if(attrs.buttonid == "rotateFront"){
                            //alert("rotate male front to back");
                            var statename = 'symptoms/male_back';
                            var imgurl = 'lib/image/avatar/male_front_back_180.jpg';
                            rotateImg(statename, imgurl);
                            attrs.buttonid = "rotateBack";


                        }else if(attrs.buttonid == "rotateBack"){
                            //alert("rotate it front");
                            var statename = 'symptoms/male';
                            var imgurl = 'lib/image/avatar/male_back_front_180.jpg';
                            rotateImg(statename, imgurl);
                            attrs.buttonid = "rotateFront";
                            /*$timeout(function(){
                                scope.hideImageMaleFront = false;
                            }, 1000);*/
                        }else if(attrs.buttonid == "rotateFemaleFront"){
                            //alert("rotate female front to back");
                            var statename = 'symptoms/female_back';
                            var imgurl = 'lib/image/avatar/female_front_back_180.jpg';
                            rotateImg(statename, imgurl);
                            attrs.buttonid = "rotateFemaleBack";
                        }else if(attrs.buttonid == "rotateFemaleBack"){
                            //alert("rotate female back to front");
                            var statename = 'symptoms/female';
                            var imgurl = 'lib/image/avatar/female_back_front_180.jpg';
                            rotateImg(statename, imgurl);
                            attrs.buttonid = "rotateFemaleFront";

                        }
               };


                function rotateImg(statename, imgurl){
                        var coin,
                            coinImage,
                            canvas;
                        var i =0;
                        function gameLoop () {
                            if (i<20){
                                 window.requestAnimationFrame (gameLoop);
                                i++;
                            }else{
                                i =0;
                            }
                          //
                          coin.update();
                          coin.render();
                        }

                        function sprite (options) {

                            var that = {},
                                frameIndex = 0,
                                tickCount = 0,
                                ticksPerFrame = options.ticksPerFrame || 0,
                                numberOfFrames = options.numberOfFrames || 1;

                            that.context = options.context;
                            that.width = options.width;
                            that.height = options.height;
                            that.image = options.image;

                            that.update = function () {

                                tickCount += 1;

                                if (tickCount > ticksPerFrame) {

                                    tickCount = 0;

                                    // If the current frame index is in range
                                    if (frameIndex < numberOfFrames - 1) {
                                        // Go to the next frame
                                        frameIndex += 1;
                                    } else {
                                        //frameIndex = 0;
                                    }
                                }

                            };

                            that.render = function () {

                              // Clear the canvas
                              that.context.clearRect(0, 0, that.width, that.height);
                            //alert(that.iamge + " " + that.width +" " + that.height +" " +frameIndex +" " + numberOfFrames);
                              // Draw the animation
                              console.log(frameIndex);
                              that.context.drawImage(
                                that.image,
                                frameIndex * that.width / numberOfFrames,
                                0,
                                that.width / numberOfFrames ,
                                that.height,
                                0,
                                0,
                                imgWidth,
                                imgHeight+7);
                            };

                            return that;
                        }

                        // Get canvas
                        var height = window.innerHeight -171;
                        //alert(window.screen.availHeight);
                        var imgWidth = null;
                        var imgHeight= null;
                        if(window.screen.availHeight >= 900){
                            imgWidth = 308;
                            imgHeight = 621;
                        }else{
                            imgHeight = 504;
                            imgWidth = 250;
                        }
                       // alert(imgHeight + " " + imgWidth);
                        canvas = document.getElementById("coinAnimation");
                        canvas.width = imgWidth;
                        canvas.height = height;

                        // Create sprite sheet
                        coinImage = new Image();

                        // Create sprite
                        coin = sprite({
                            context: canvas.getContext("2d"),
                            width: 3777,
                            height: 700,
                            image: coinImage,
                            numberOfFrames: 11,
                            ticksPerFrame: 1
                        });

                        // Load sprite sheet
                        coinImage.addEventListener("load", gameLoop);
                        coinImage.src = $rootScope.static_path + imgurl;
                };


                (function() {
                    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
                    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
                    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
                    // MIT license

                    var lastTime = 0;
                    var vendors = ['ms', 'moz', 'webkit', 'o'];
                    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
                    }

                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = function(callback, element) {
                            var currTime = new Date().getTime();
                            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                              timeToCall);
                            lastTime = currTime + timeToCall;
                            return id;
                        };

                    if (!window.cancelAnimationFrame)
                        window.cancelAnimationFrame = function(id) {
                            clearTimeout(id);
                        };
                }());



            }
        }

    }
})();





