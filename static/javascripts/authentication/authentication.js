/**
 * Created by IDCS12 on 5/22/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.authentication',[
        'myApp.authentication.controller',
        'myApp.authentication.service'
    ]);

    angular.module('myApp.authentication.controller',[]);

    angular.module('myApp.authentication.service',[]);
})();