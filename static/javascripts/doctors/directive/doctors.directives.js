/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.doctors.directives')
        .directive('myMaps', myMapsFun);



    function myMapsFun(){
        return{
          restrict:'E',
          template :'<div></div>',
          scope : {
            latlog : '@'
          },
          replace:'true',
          link:function(scope,element,attrs){

            var offsetValue_arr = [];

            scope.$watch('latlog', function() {
              changLatLog();
            });

            function changLatLog()
            {

              var latlog = JSON.parse(scope.latlog);
              for(var m = offsetValue_arr.length; m<latlog.length; m++)
              {
                offsetValue_arr.push(latlog[m].offset_top);
              }

              var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
              var icons =
                [
                    iconURLPrefix + 'red-dot.png',
                    iconURLPrefix + 'green-dot.png',
                    iconURLPrefix + 'blue-dot.png',
                    iconURLPrefix + 'orange-dot.png',
                    iconURLPrefix + 'purple-dot.png',
                    iconURLPrefix + 'pink-dot.png',
                    iconURLPrefix + 'yellow-dot.png'
                  ];
              var iconsLength = icons.length;

              var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    zoom: 10,
                    center: new google.maps.LatLng(-37.92, 151.25),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: true,
                    streetViewControl: false,
                    panControl: false,
                    zoomControlOptions: {
                       position: google.maps.ControlPosition.LEFT_BOTTOM
                    }
              });
              var infowindow = new google.maps.InfoWindow();

              var markers = new Array();
              var iconCounter = 0;
              var i=0;
              angular.forEach(latlog, function(key, value){

                angular.forEach(key.latitude, function(key1, value1){
                    var marker = new google.maps.Marker({
                      position: new google.maps.LatLng(parseFloat(key1), parseFloat(key.longitude[value1])),
                      map: map,
                      icon: icons[iconCounter]

                      });
                      google.maps.event.addListener(marker, 'click', function(){
                        infowindow.setContent(key.doctor_name_for_map);

                        infowindow.open(map, this);

                        showCurrentItem(offsetValue_arr[value]);
                      });
                      markers.push(marker);
                      /*google.maps.event.addListener(marker, 'click', (function(marker, i) {

                      })(marker, i));*/
                      iconCounter++;
                      // We only have a limited number of possible icon colors, so we may have to restart the counter
                      if(iconCounter >= iconsLength) {
                        iconCounter = 0;
                      }


                    i++;
                });

              });

              autoCenter();

              function showCurrentItem(offset_top)
              {
                $(".doctor_list").animate({scrollTop:offset_top}, 500);
              }

              function autoCenter()
              {

                  //  Create a new viewpoint bound
                  var bounds = new google.maps.LatLngBounds();
                  //  Go through each...
                  for (var i = 0; i < markers.length; i++) {
                    bounds.extend(markers[i].position);
                  }
                  //  Fit these bounds to the map
                  map.fitBounds(bounds);
              }

              function stopAnimation(marker1)
              {
                  setTimeout(function ()
                  {
                      marker.setAnimation(null);
                  }, 30000);
              }
              function displayhovermap(lt,lg)// when the user does the hovering on the address this function will be called.
              {
                // Add the markers and infowindows to the map

                while(markers.length)// clear all the markers that are already on the map
                {
                  markers.pop().setMap(null);
                }

                angular.forEach(lt, function(key, value)
                {
                  var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(key), parseFloat(lg[value])),
                    draggable:true,
                    animation: google.maps.Animation.BOUNCE,//make the marker to bounce
                    map: map,
                    icon: icons[iconCounter]
                  });
                  markers.push(marker);
                  
                  iconCounter++;
                  // We only have a limited number of possible icon colors, so we may have to restart the counter
                  if(iconCounter >= iconsLength) {
                    iconCounter = 0;
                  }
                });

            }
            scope.$on("DoctorMouseOutEvent",function()
            {
                while(markers.length)//clear all the previous markers that are already there on the map
                {
                  markers.pop().setMap(null);
                }
                angular.forEach(latlog, function(key, value)
                {
                  angular.forEach(key.latitude, function(key1, value1){

                    var marker = new google.maps.Marker({
                      position: new google.maps.LatLng(parseFloat(key1), parseFloat(key.longitude[value1])),
                      map: map,
                      icon: icons[iconCounter]
                    }
                    );
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'click', function(){
                      infowindow.setContent(key.doctor_name_for_map);
                      infowindow.open(map, this);
                      showCurrentItem(offsetValue_arr[value]);
                    });

                    iconCounter++;
                    if(iconCounter >= iconsLength) {
                      iconCounter = 0;
                    }
                    i++;
                  });
                });
            });

            scope.$on("DoctorMouseFocusEvent",function(event, map_value){
              /*angular.forEach(latlog, function(key, value)
              {
                    if(parseFloat(key.latitude)==parseFloat(latitude_.latitude))
                    {
                      i=0;
                      displayhovermap(latitude_.latitude,key.longitude,i);
                    }
              });*/
              displayhovermap(map_value.latitude, map_value.longitude);
            });
          }
        }
      }
    }



})();