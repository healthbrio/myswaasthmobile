/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var doctor_service=angular.module('myApp.doctors.services');
    doctor_service.factory('DoctorsService', doctorsServiceFun);
    doctor_service.factory('DoctorsServiceList', doctorsServiceListFun);
    doctor_service.factory('DoctorsDetailsList', doctorsDetailsListFun);
    doctorsServiceFun.$inject=['HttpService'];
    doctorsServiceListFun.$inject=['HttpService'];
    doctorsDetailsListFun.$inject=['HttpService'];
    
    function doctorsServiceFun(HttpService)
    {
        var doctorApiSerivce  = {
                all : all,
                locationFind : locationFind,
                filterService : filterService
            }

            function all(offsetValue){
                
                return (HttpService.PublicServiceURL('api/doctors/?offset='+offsetValue));
            }
            function locationFind(location_, page_){
                
                if(location_.location==null || location_.location=="")
                {    
                    return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city+"&page="+page_));
                }
                else
                {
                    return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city+"&locality="+location_.location+"&page="+page_));
                }
            }

            function filterService(filter_, location_){
                
                if(location_.location==null || location_.location=="")
                {    
                    return (HttpService.PublicServiceURL('search_doctors/type/?city='+location_.city+'&key='+filter_));
                }
                else
                {
                    return (HttpService.PublicServiceURL('search_doctors/type/?city='+location_.city+'&locality='+location_.location+'&key='+filter_));
                }
            }

        return doctorApiSerivce;
    }

    function doctorsServiceListFun(HttpService)
    {
        var doctorApiSerivce  = {
                all : all
        }

        function all(slug, location_, page_, locality){

            if(locality === '' || locality===undefined)
            {
                if(location_.location == null || location_.location == ""){
                    return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city+"&speciality="+slug+"&page="+page_));
                }else{
                    return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city+"&locality="+location_.location+"&speciality="+slug+"&page="+page_));
                }
            }
            else
            {
                if(location_.location == null || location_.location == "" || location_.location == undefined){
                    return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city +"&detail="+locality+"&speciality="+slug+"&page="+page_));
                }else{
                    return(HttpService.PublicServiceURL('search_doctors/?city='+ location_.city + "&locality=" +location_.location +"&detail="+ locality +"&speciality=" +slug+"&page="+page_));
                }
            }

            
        }

        function doctorListFilterByLocation(slug, location_, locality)
        {
            return (HttpService.PublicServiceURL('search_doctors/?city='+location_.city+"&detail="+locality+"&speciality="+slug));
        }

        return doctorApiSerivce;
    }

    function doctorsDetailsListFun(HttpService){
        var doctorApiDetails  = {
                all : all
        }
        function all(slug){
            return (HttpService.PublicServiceURL("search_doctors/?id="+slug));
        }

        return doctorApiDetails;
    }

})();