/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.doctors.controller')
        .controller('DoctorsController', DoctorsControllerFun);
	angular.module('myApp.doctors.controller')
        .controller('DoctorsOverviewController', DoctorsOverviewControllerFun);
    angular.module('myApp.doctors.controller')
        .controller('DoctorsDetailsController', DoctorsDetailsControllerFun);



    DoctorsControllerFun.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$http', '$interval', '$timeout', '$window', 'DoctorsService', 'ScrollService', 'CacheService', 'GeoLocationService'];
	DoctorsOverviewControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', '$timeout', '$window', 'ScrollService', 'CacheService', 'DoctorsServiceList'];
    DoctorsDetailsControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', '$window', 'ScrollService', 'DoctorsDetailsList'];

    function DoctorsControllerFun($scope, $rootScope, $state, $stateParams, $http, $interval, $timeout, $window, DoctorsService, ScrollService, CacheService, GeoLocationService)
    {

        var vm=this;
        vm.specilityList=[];

        vm.isSearch_box = false;

        vm.isDisabled=false;

        vm.isLoading=true;
        vm.isMapLoading=false;

        vm.busy = false;

        vm.after = 1;

        vm.countover = true;

        var isloaded=true;

        vm.isFilter=false;

        if(CacheService.isCache("user_location"))
        {
            $rootScope.isUserLocationFind=true;
            var location_={
                city : CacheService.getCache("user_location").user_city_slug,
                location : CacheService.getCache("user_location").user_location_slug
            }

            if (CacheService.isCache("speciality_cache")) {

                vm.specilityList=[];
                var data = CacheService.getCache("speciality_cache");
                isloaded=false;
                var i=0;
                vm.isLoading=true;

                $timeout(function(){
                    vm.isLoading=false;
                    var interval=$interval(function(){

                        vm.specilityList.push(data.list[i]);
                        i++;
                        
                        if(i==data.list.length)
                        {
                            isloaded=true;
                            $interval.cancel(interval);
                        }
                        
                    }, 10);
                }, 1500);
                    window.isRefresh==false;
                    vm.after = data.after;
                    vm.busy = false;
            }

            vm.loadMore = function () {

                if (vm.busy || !isloaded) return;
                if(vm.isFilter) return;

                if (vm.countover) {
                    vm.busy = true;
                    DoctorsService.locationFind(location_, vm.after).success(function (response) {
                        
                        vm.isLoading=false;
                        vm.busy = false;
                        var data = response[0].speciality;
                        for (var i = 0; i < data.length; i++) {
                            vm.specilityList.push(data[i]);
                        }

                        vm.after = vm.after + 1;

                        var data1 = {
                            list: vm.specilityList,
                            after: vm.after
                        };
                        
                        if (vm.after > response[1].last_page) {
                            vm.countover = false;
                        } else {
                            console.log("true countover ");
                        }
                    }.bind(this)).error(function () {
                        vm.busy = false;
                        vm.isLoading=false;
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                    });

                }
            };

            vm.searchButton = function () {
                    vm.isSearch_box = !vm.isSearch_box;
                }
     
            var timeout, interval;
            vm.filterListChange=function(filterList)
            {
                
                vm.isFilter=true;
                vm.specilityList=[];
                
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                    

                if(filterList!="")
                {
                    vm.isLoading=true;
                    timeout=$timeout(function(){
                        vm.isDisabled=true;
                        vm.isFilter=true;
                        DoctorsService.filterService(filterList, location_).success(function (response) {
                            vm.specilityList=response;
                            vm.isDisabled=false;
                            vm.isLoading=false;
                        }).error(function(error){
                            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                            vm.isDisabled=false;
                            vm.isLoading=false;
                        });
                    }, 500);
                }

                if(filterList=="")
                {
                    vm.isFilter = false;
                    vm.countover = true;
                    vm.after = 0;
                    vm.after = 1;
                    vm.loadMore();
                    /*$timeout.cancel(timeout);
                    vm.isLoading=false;
                    if (CacheService.isCache("speciality_cache")) {
                    var data = CacheService.getCache("speciality_cache");
                    isloaded=false;
                    var i=0;
                    interval=$interval(function(){

                        vm.specilityList.push(data.list[i]);
                        i++;
                        
                        if(i==data.list.length)
                        {
                            isloaded=true;
                            vm.isFilter=false;
                            $interval.cancel(interval);
                        }
                        
                    }, 10);
                    vm.after = data.after;
                    }*/
                }
            }
        }

        vm.findMyCurrentLocation = function()
        {
            $scope.isUserLocationFind=!$scope.isUserLocationFind;
        }
        $scope.$on("LocationChangesEvent", function($event){
            $state.transitionTo($state.current, {doctors_params : $stateParams.doctors_params+1}, { reload: false, inherit: true, notify: true }); 
        });

        vm.closeView=function(){
            try{
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".specialityListFromWCIG")));
            }catch(error){
                $state.go("home");
            }

            ScrollService.scrollBack();
        };

        if($state.current.name === "doctors")
        {
            ScrollService.scrollBegin();
        }
        else
        {
            ScrollService.scrollStart();
        }
    }



    function DoctorsOverviewControllerFun($scope, $http, $stateParams, $state, $timeout, $window, ScrollService, CacheService, DoctorsServiceList)
    {
        var vm = this;
        vm.doctorsList=[];
        vm.locationList=[];

        vm.isLoading=true;
        vm.title="";
        vm.localitydiv =true;
        vm.isMouseOver=false;
        vm.isMouseClick=false;
        vm.doctors_map_array=new Array();

        vm.busy = false;

        vm.after = 1;

        vm.countover = true;

        var isloaded=true;

        vm.location_name = "";

        var locality = "";



        var location_={
            city : CacheService.getCache("user_location").user_city_slug,
            location : CacheService.getCache("user_location").user_location_slug
        }
        if(location_.location){
            vm.location_name = location_.location+", "+location_.city;
        }else{
            vm.location_name = location_.city;
        }

        vm.loadMore = function () {
            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;

            if (vm.countover) {
                vm.busy = true;
                DoctorsServiceList.all($stateParams.specility_id, location_, vm.after, locality).success(function (response) {
                    
                    var data=response[0].speciality;
                    if(vm.after===1 && locality==='')
                    {
                        angular.forEach(response[2].location, function(key, value){
                            key.isChecked=false;
                            vm.locationList.push(key);
                        });
                        vm.title = response[3].title[0];
                    }
                    vm.isLoading=false;
                    
                    for (var i = 0; i < data.length; i++) {
                        vm.doctorsList.push(data[i]);
                    }

                    setGoogleMap();
                    vm.after = vm.after + 1;

                    vm.busy = false;
                    
                    if (vm.after > response[1].last_page) {
                        vm.countover = false;
                    }
                }.bind(this)).error(function () {
                    vm.busy = false;
                    vm.isLoading=false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        /**********add Doctors Maps Value******/
        function setGoogleMap()
        {
            $timeout(function(){
                vm.doctors_map_array = [];
                angular.forEach(vm.doctorsList, function(key, value)
                {
                    angular.forEach(key.clinic, function(key1, value1){
                        
                        var latitude_value=key1.main_facility.address.latitude, longitude_value=key1.main_facility.address.longitude;
                        var ll_arr={
                            latitude : [latitude_value],
                            longitude : [longitude_value],
                            offset_top : parseInt($(".doctor_list_card").eq(value).offset().top)-163,
                            doctor_name_for_map : (key.owner.first_name==null?'':key.owner.first_name)+" "+(key.owner.middle_name==null?'':key.owner.middle_name)+" "+(key.owner.last_name==null?'':key.owner.last_name)
                        };
                        if(latitude_value && longitude_value){
                            vm.doctors_map_array.push(ll_arr);
                        }
                    });
                    vm.isMapLoading=true;
                });
            });
        }
        
        vm.showLocalityData = function(){
            vm.localitydiv = vm.localitydiv ? false : true;
            $scope.isUserLocationFind=false;
        };

        vm.doctorMouseFocus = function(clinic){
            var latitude_ = [];
            var longitude_ = [];
            angular.forEach(clinic, function(key, value){
                latitude_.push(key.main_facility.address.latitude);
                longitude_.push(key.main_facility.address.longitude);
            });
            $scope.$broadcast("DoctorMouseFocusEvent", {latitude : latitude_, longitude : longitude_});
        };
        vm.doctorMouseOut = function(){
            $scope.$broadcast("DoctorMouseOutEvent");
        };

        vm.localitySelect = function(location_list){
            
            locality = "";
            
            angular.forEach(location_list, function(key, value){
                if(key.isChecked){
                    
                    locality+= key.slug+",";
                }
            });
            locality = locality.substring(0, locality.lastIndexOf(","));
            
            vm.after = 1;
            vm.countover = true;
            vm.doctorsList = [];
            vm.loadMore();
        };

        vm.closeView=function(){
            
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".doctor_list")));
            ScrollService.scrollBack();
        };

        ScrollService.scrollStart();
    }


    function DoctorsDetailsControllerFun($scope, $http, $stateParams, $state, $window, ScrollService, DoctorsDetailsList){
        var vm = this;
        $scope.state = $state;
        vm.doctorsDetails=[];
        vm.summary = "";
        vm.isLoading=true;
        vm.doctor_id = $stateParams.doctor_id;


        DoctorsDetailsList.all($stateParams.doctor_id).success(function(response){
            vm.doctorsDetails=response;
            vm.isLoading=false;
        }).error(function(error){
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            vm.isLoading=false;
        });

        //alert(angular.element(document.getElementsByClassName('comment')[0]).children());
        //var summary = document.getElementsByClassName('avatar')[0];
        //var content = angular.element(summary).html();


        vm.closeView=function(){
            if($state.current.name.indexOf(".doctor_overview")!=-1)
            {
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".doctor_overview")));
            }
            else
            {
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".overview")));
            }
            ScrollService.scrollBack();
        };

        vm.scrollToClinic = function(hospital_slug)
        {
            $scope.reloadParentViewHospital(hospital_slug);
            ScrollService.scrollBack();
        }

        $scope.reloadParentViewDoctor = function(doctor_id)
        {
            vm.isLoading = true;
            DoctorsDetailsList.all(doctor_id).success(function(response){
                vm.doctorsDetails=response;
                vm.isLoading=false;
            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                vm.isLoading=false;
            });
            ScrollService.scrollTop(".doctor_details_list");
        }
        
        ScrollService.scrollStart();
    }
    
})();

