(function(){
    'use strict';

    var auth_service=angular.module('myApp.geoService', []);
    auth_service.service('GeoLocationService', geoLocationServiceFun);
    geoLocationServiceFun.$inject=['$rootScope', 'HttpService'];
    function geoLocationServiceFun($rootScope, HttpService)
    {

    	var lat, lon, latlon, geocoder;
        this.getCurrentLocation=function(){
        	if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(showPosition, showError,{maximumAge:0, timeout:500000, enableHighAccuracy:true});
		    }
        }

        function showPosition(position) {
		    lat = position.coords.latitude;
		    lon = position.coords.longitude;
		    latlon = new google.maps.LatLng(lat, lon);
		    geocoder = new google.maps.Geocoder();
 			codeLatLng(lat, lon);

		}

		function codeLatLng(lat, lon) {

		    geocoder.geocode({'latLng': latlon}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {
			        //Check result 0
			        var result = results[0];
			        //look for locality tag and administrative_area_level_1
			        var city = "";
			        var state = "";
			        var locality = "";
			        for(var i=0, len=result.address_components.length; i<len; i++) {
			          var ac = result.address_components[i];
			          if(ac.types.indexOf("locality") >= 0) city = ac.long_name;
			          if(ac.types.indexOf("administrative_area_level_1") >= 0) state = ac.long_name;
			          if(ac.types.indexOf("sublocality_level_1") >= 0) locality = ac.long_name;
			        }
			        //only report if we got Good Stuff
			        if(city != '' && state != '') {
			        	var geoObj={
			        		city : city,
			        		state : state,
			        		locality : locality
			        	}
			         	$rootScope.$broadcast("GeoLocationEvent", geoObj);
			        }
			        else
			        {
			        	$rootScope.alerts[0]={ type: 'danger', msg: 'We are not getting your Current Location.' };
			        }
				}
			});
		}

		function showError(error) {
		    $rootScope.alerts[0]={ type: 'danger', msg: 'We are not getting your Current Location.' };
		}

		//***************Getting City & Locality List from Server*****
		this.getCityList = function()
		{
			return HttpService.PublicServiceURL("get_list/city/");
		}
		this.getLocalityList = function(city_name, locality_char)
		{
			return HttpService.PublicServiceURL("get_list/locality/?city="+city_name+"&local_key="+locality_char);
		}

    }

})();