(function(){
    'use strict';

    var auth_service=angular.module('myApp.authService', []);
    auth_service.service('AuthService', authServiceFun);
    authServiceFun.$inject=['$rootScope', '$http', 'CacheService', '$timeout', 'myConditionService', 'myMedicationService', 'myProcedureService'];
    function authServiceFun($rootScope, $http, CacheService, $timeout, myConditionService, myMedicationService, myProcedureService)
    {

        this.sendFBToken=function(token){
        	$http({
		        url:$rootScope.static_server_path+'client/fb/',
		        method:'POST',
		        headers:{
		            'Content-Type':'application/json'
		        },
		        data : {
		        	"access_token":token
		        }
		      }).success(function(response){
		      	CacheService.setCache("mySwaasthToken", JSON.stringify(response));
		      	getClientInfoBySocialNetwork(response);
		      }).error(function(error){
		        alert(JSON.stringify(error));
		      });
        }

        this.sendGoogleToken=function(token){
        	$http({
		        url:$rootScope.static_server_path+'client/google/',
		        method:'POST',
		        headers:{
		            'Content-Type':'application/json'
		        },
		        data : {
		        	"access_token":token
		        }
		      }).success(function(response){
		      	CacheService.setCache("client_secret", JSON.stringify(response));
		      	getClientInfoBySocialNetwork(response);
		      }).error(function(error){
		        $rootScope.alerts[0]={ type: 'danger', msg: 'Oh snap! Some Problems with Google+ Login' };
		      });
        }

        this.sendSwaasthToken=function(token){
        	CacheService.setCache("client_secret", JSON.stringify(token));
        	getClientInfo(token[0]);
        }

        this.changePassword=function(old_pass, new_pass)
        {
        	var client=(JSON.parse(CacheService.getCache("client_secret")).length>0?JSON.parse(CacheService.getCache("client_secret"))[0]:JSON.parse(CacheService.getCache("client_secret")));
        	return ($http({
		        url:$rootScope.static_server_path+'client/patient/'+client.info.patient_id,
		        method:'PUT',
		        headers:{
		            'Content-Type':'application/json',
	          		'Authorization' : client.token.token_type+' '+client.token.access_token
		          	},
		        data:{
		        	"old_password" : old_pass,
		        	"new_password" : new_pass
		        }

		        }));
        }

        function getClientInfoBySocialNetwork(data){

        	$http({
		        url:$rootScope.static_server_path+'client/patient_myprofile_detail/'+data.info.my_profile_id,
		        method:'GET',
		        headers:{
		            'Content-Type':'application/json',
	          		'Authorization' : data.token.token_type+' '+data.token.access_token
		          	}
		        }).success(function(response){
		        	CacheService.setCache("client_info", JSON.stringify(response));
		        	
		        	//**********get All My Condition, My Medication, My Procedure List*****
		        	myConditionService.all().success(function(response){
			            CacheService.setCache("myConditionList", response);
			        });
			        myMedicationService.all().success(function(response){
			            CacheService.setCache("myMedicationList", response);
			        });
			        myProcedureService.all().success(function(response){
			            CacheService.setCache("myProcedureList", response);
			        });

		        	$rootScope.$broadcast('LoginEvent', response);
		        }).error(function(error){
		        	$rootScope.alerts[0]={ type: 'danger', msg: 'Oh snap! Some Problems with Social Login' };
		    	});
    	}

    	function getClientInfo(data){
    		$http({
		        url:$rootScope.static_server_path+'client/patient_myprofile_detail/'+data.info.my_profile_id,
		        method:'GET',
		        headers:{
		            'Content-Type':'application/json',
	          		'Authorization' : data.token.token_type+' '+data.token.access_token
		          	}
		        }).success(function(response){
		        	CacheService.setCache("client_info", JSON.stringify(response));
		        	
		        	//**********get All My Condition, My Medication, My Procedure List*****
		        	myConditionService.all().success(function(response){
			            CacheService.setCache("myConditionList", response);
			        });
			        myMedicationService.all().success(function(response){
			            CacheService.setCache("myMedicationList", response);
			        });
			        myProcedureService.all().success(function(response){
			            CacheService.setCache("myProcedureList", response);
			        });

		        	$rootScope.$broadcast('LoginEvent', response);
		        	$rootScope.alerts[0]={ type: 'success', msg: "Welcome! "+ response.first_name+" ("+response.email+")" };
		        }).error(function(error){
		        	$rootScope.alerts[0]={ type: 'danger', msg: 'Oh snap! You not Authorize Person' };
		    	});
    	}

    	this.refreshLogin = function(){


    		var client_s=JSON.parse(CacheService.getCache("client_secret"));
            var client_i=JSON.parse(CacheService.getCache("client_info"));
            var client_id = client_s[0].info.client_id;
            var client_secret = client_s[0].info.client_secret;
            var email =  client_i.email;
            var r_token = client_s[0].token.refresh_token;
            $http({
		        url:$rootScope.static_server_path+'o/token/?client_id='+client_id+"&client_secret="+client_secret+"&email="+email+"&grant_type=refresh_token&refresh_token="+r_token,
		        method:'POST',
		        headers:{
		            'Content-Type':'application/x-www-form-urlencoded',
	          		'Authorization' : client_s[0].token.token_type+' '+client_s[0].token.access_token
		          	}
		        }).success(function(response){
		        	client_s[0].token.access_token=response.access_token;
		        	client_s[0].token.token_type=response.token_type;
		        	client_s[0].token.expires_in=response.expires_in;
		        	client_s[0].token.refresh_token=response.refresh_token;
		        	client_s[0].token.scope=response.scope;
		        	var refresh_string=JSON.stringify(client_s).replace(/"/g, '\\"');
		        	window.sessionStorage.setItem("client_secret", '"'+refresh_string+'"');
		        }).error(function(error){
		        	$rootScope.alerts[0]={ type: 'danger', msg: 'Oh snap! You not Authorize Person' };
		        });
    	}

    }

})();