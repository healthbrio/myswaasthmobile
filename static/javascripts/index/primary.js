/**
 * Created by IDCS12 on 3/23/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.primary',[
        'myApp.primary.controller',
        'myApp.primary.directives',
        'myApp.primary.services'
    ]);


    console.log("primary getter")
    angular.module('myApp.primary.controller',[]);

    angular.module('myApp.primary.directives',[]);

    angular.module('myApp.primary.services',[]);
})();