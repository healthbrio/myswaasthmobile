/**
 * Created by IDCS12 on 3/23/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.WhereCouldIGo',[
        'myApp.WhereCouldIGo.controller',
        'myApp.WhereCouldIGo.directives',
        'myApp.WhereCouldIGo.services'
    ]);


    console.log("WhereCouldIGo getter")
    angular.module('myApp.WhereCouldIGo.controller',[]);

    angular.module('myApp.WhereCouldIGo.directives',[]);

    angular.module('myApp.WhereCouldIGo.services',[]);
})();