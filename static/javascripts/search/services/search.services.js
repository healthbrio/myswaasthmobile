/**
 * Created by IDCS12 on 6/12/2015.
 */
(function(){
    'use strict';
    var search_service=angular.module('myApp.search.services');
    search_service.factory('MainSearchService', MainSearchServiceFun);

    MainSearchServiceFun.$inject=['HttpService'];

    function MainSearchServiceFun(HttpService)
    {
        var searchApiSerivce  = {
            all : all,
            hotSearch : hotSearch
        };

        function all(offsetValue){
            return (HttpService.PublicServiceURL('searchnow/?query=' +offsetValue));
        }

        function hotSearch(offsetValue){
            return (HttpService.PublicServiceURL('search_on_type/?query=' +offsetValue));
        }
            
        return searchApiSerivce;
    }

})();