/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var hospital_service=angular.module('myApp.hospitals.services');
    hospital_service.factory('HospitalsService', HospitalsServiceFun);
    hospital_service.factory('HospitalsServiceList', hospitalServiceListFun);
    hospital_service.factory('HospitalsDetailsList',HospitalsDetailsListFun);


    HospitalsServiceFun.$inject=['HttpService'];
    hospitalServiceListFun.$inject = ['HttpService'];
    HospitalsDetailsListFun.$inject = ['HttpService'];
    
    function HospitalsServiceFun(HttpService)
    {
        var hospitalApiSerivce  = {
            all : all
        }

        function all(){
            return (HttpService.PublicServiceURL('api/facility/'));
        }

        return hospitalApiSerivce;
    }

    function hospitalServiceListFun(HttpService){
        var hospitalApiService = {
            all : all
        }

        function all(slug, location_, page_, locality){
            
            if(locality === '' || locality===undefined)
            {
                if(location_.location == null || location_.location == ""){
                    return(HttpService.PublicServiceURL('api/facility/get_hospital/?city='+location_.city+"&type="+slug+"&page="+page_));
                }else{
                    return(HttpService.PublicServiceURL('api/facility/get_hospital/?city='+location_.city+"&locality="+location_.location+"&type="+slug+"&page="+page_));
                }
            }
            else
            {
                if(location_.location == null || location_.location == "" || location_.location == undefined){
                    return (HttpService.PublicServiceURL('api/facility/get_hospital/?city='+location_.city +"&detail="+locality+"&type="+slug+"&page="+page_));
                }else{
                    return(HttpService.PublicServiceURL('api/facility/get_hospital/?city='+ location_.city + "&locality=" +location_.location +"&detail="+ locality +"&type=" +slug+"&page="+page_));
                }
            }
        }

        return hospitalApiService;
    }



    function HospitalsDetailsListFun(HttpService){
        var hospitalApiDetails = {
            all :all
        }
        function all(slug){
            return (HttpService.PublicServiceURL('api/facility/hospital_detail/?clinic='+slug));
        }
        return hospitalApiDetails;
    }
})();