/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.hospitals.controller')
        .controller('HospitalsController', HospitalsControllerFun);

    angular.module('myApp.hospitals.controller')
        .controller('HospitalOverviewController', HospitalOverviewControllerFun);

    angular.module('myApp.hospitals.controller')
        .controller('HospitalsDetailsController', HospitalsDetailsControllerFun);


	

    HospitalsControllerFun.$inject = ['$scope', '$state', '$rootScope', 'HospitalsService', 'ScrollService', 'CacheService'];
    HospitalOverviewControllerFun.$inject = ['$scope', '$state', '$stateParams', 'ScrollService', 'CacheService', 'HospitalsServiceList'];
    HospitalsDetailsControllerFun.$inject = ['$scope', '$state', '$stateParams', 'ScrollService', 'CacheService', 'HospitalsDetailsList'];

    function HospitalsControllerFun($scope, $state, $rootScope, HospitalsService, ScrollService, CacheService){
        var vm=this;
        vm.hospitalList=[];
        vm.isLoading=true;


        if(CacheService.isCache("user_location")){
            $rootScope.isUserLocationFind =true;
            var location = {
                city : CacheService.getCache("user_location").user_city_slug,
                location : CacheService.getCache("user_location").user_location_slug
            }
        }


        vm.findMyCurrentLocation = function(){
            $scope.isUserLocationFind=!$scope.isUserLocationFind;
        }

        HospitalsService.all().success(function(response){
            vm.isLoading=false;
            vm.hospitalList = response;
        }).error(function(){
            vm.isLoading=false;
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
        });

        vm.closeView = function(){
            $state.go("home");
        }

        $scope.$on("LocationChangesEvent", function($event){
            $scope.isUserLocationFind=true;
            //$state.transitionTo($state.current, {doctors_params : $stateParams.doctors_params+1}, { reload: false, inherit: true, notify: true }); 
        });
        ScrollService.scrollBegin();
    }

    function HospitalOverviewControllerFun($scope, $state, $stateParams, ScrollService, CacheService, HospitalsServiceList){
        var vm =this;
        vm.hospitalList = [];
        vm.locationList = [];

        vm.isLoading = true;
        vm.title =  $stateParams.hospital_slug.replace(/-/g," ");

        vm.localitydiv = true;
        vm.isMouseOver  = false;
        vm.isMouseClick = false;
        vm.doctors_map_array=new Array();
        vm.busy = false;
        vm.after = 1;
        vm.countover = true;
        var isloaded = true;
        vm.location_name = "";

        var locality = "";

        var location_={
            city : CacheService.getCache("user_location").user_city_slug,
            location : CacheService.getCache("user_location").user_location_slug
        };

        if(location_.location){
            vm.location_name = location_.location+", "+location_.city;
        }else{
            vm.location_name = location_.city;
        }

        
        vm.loadMore = function () {
            if (vm.busy || !isloaded) return;
            
            if (vm.countover) {
                vm.busy = true;
                HospitalsServiceList.all($stateParams.hospital_slug, location_, vm.after, locality).success(function (response) {
                    var data = response[0].hospital_detail;
                    
                    vm.isLoading = false;
                    if(vm.after == 1 && locality===''){
                        angular.forEach(response[1].location, function(key, value){
                            key.isChecked = false;
                            vm.locationList.push(key);
                        });
                        vm.title = response[3].title[0];
                    }

                    if(locality==='')
                    {
                        for(var i =0; i <data.length; i++){
                            vm.hospitalList.push(data[i]);
                        }
                    }
                    else
                    {
                        for(var i =0; i <data.length; i++){
                            var filter_modify_json = {
                                name : data[i].name,
                                slug : data[i].slug,
                                address : {
                                    street_address : data[i].address,
                                    locality : {
                                        name : data[i].locality,
                                        city : {
                                            name : data[i].city
                                        }
                                    },
                                    latitude : data[i].latitude,
                                    longitude : data[i].longitude,
                                    url : "",
                                    mobile : data[i].mobile,
                                    med_image : "",
                                    short_image : data[i].short_image
                                },
                                practic_info : data[i].practic_info
                            }
                            vm.hospitalList.push(filter_modify_json);
                        }
                    }

                    setGoogleMap();
                    vm.busy = false;
                    vm.after = vm.after +1;

                    if(vm.after > response[2].last_page || vm.after > response[1].last_page){
                        vm.countover = false;
                    }

                }.bind(this)).error(function () {
                    vm.busy = false;
                    vm.isLoading=false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        /****************add hospital maps value ******************/
        function setGoogleMap(){
            
            vm.doctors_map_array = [];
            angular.forEach(vm.hospitalList, function(key, value){

                var latitude_value = key.address.latitude, longitude_value = key.address.longitude;

                //alert(latitude_value);
                var ll_arr = {
                    latitude : [latitude_value],
                    longitude :[longitude_value],
                    doctor_name_for_map : key.name
                };
                if(latitude_value && longitude_value){
                    vm.doctors_map_array.push(ll_arr);
                }
                vm.isMapLoading=true;
            });
        }

        vm.showLocalityData = function(){
            vm.localitydiv = vm.localitydiv ? false : true;
            $scope.isUserLocationFind = false;
        };

        vm.hospitalMouseFocus = function(clinic){
            var latitude_ = [];
            var longitude_ = [];
            latitude_.push(clinic.address.latitude);
            longitude_.push(clinic.address.longitude);
            $scope.$broadcast("DoctorMouseFocusEvent", {latitude : latitude_, longitude : longitude_});
        };

        vm.doctorMouseOut = function(){
            $scope.$broadcast("DoctorMouseOutEvent");
        };

        vm.localitySelect = function(location_list){
            locality = "";
            
            angular.forEach(location_list, function(key, value){
                if(key.isChecked){
                    
                    locality+= key.slug+",";
                    //locality = "nagar";
                }
            });
            locality = locality.substring(0, locality.lastIndexOf(","));
            
            vm.after = 1;
            vm.countover = true;
            vm.hospitalList = [];
            vm.loadMore();
        };

        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".hospitals_list")));
            ScrollService.scrollBack();
        };

        ScrollService.scrollStart();

    }


    function HospitalsDetailsControllerFun($scope, $state, $stateParams, ScrollService, CacheService, HospitalsDetailsList){
        var vm = this;
        vm.hospitalsDetatils = [];
        vm.isLoading = true;
        vm.title=""
        
        HospitalsDetailsList.all($stateParams.hospital_id).success(function(response){
            vm.hospitalsDetatils = response;
           // console.log(response.length);
            vm.title = vm.hospitalsDetatils[0].name;
            //console.log(vm.hospitalsDetatils[0].address.mobile);

            vm.isLoading = false;
        }).error(function(error){
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            vm.isLoading=false;
        });


        vm.closeView = function(){
            if($state.current.name.indexOf(".hospital_overview")!=-1)
            {
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".hospital_overview")));
            }
            else
            {
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".overview")));
            }
            ScrollService.scrollBack();
        }

        ScrollService.scrollStart();

        vm.scrollToClinic = function(doctor_id){
            $scope.reloadParentViewDoctor(doctor_id);
            ScrollService.scrollBack();
        };
        $scope.reloadParentViewHospital = function(hospital_slug){
            vm.isLoading = true;
            HospitalsDetailsList.all(hospital_slug).success(function(response){
                vm.hospitalsDetatils = response;
                vm.title = vm.hospitalsDetatils[0].name;
                vm.isLoading = false;
            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                vm.isLoading=false;
            });
            ScrollService.scrollTop(".hospital_details_list");
        }

    }
    
    
})();

