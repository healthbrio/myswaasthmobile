

(function () {
  'use strict';

  var app=angular
    .module('myApp', [
      //'myApp.config',

      'ngAnimate',
      'myApp.search',
      'myApp.primary',
      'myApp.authentication',
      'myApp.routes',
      'myApp.symptoms',
      //'myApp.conditions'
      'myApp.conditions',
      'myApp.medications',
      'myApp.procedures',
      'myApp.doctors',
      'myApp.hospitals',
      'myApp.dashboard',
      'myApp.WhereCouldIGo',

      'myApp.publicService',
      'myApp.scrollService',
      'myApp.cacheService',
      'myApp.authService',
      'myApp.geoService',
      'perfect_scrollbar',
      'uiSwitch',
      /*'ngSlider',*/
      /*'ui.checkbox',*/
      'ngMaterial',
      'ngMdIcons',
      'youtube-embed',
      'facebook',
      '720kb.socialshare',
      'ngClipboard',
      'sticky'



      /*'headroom'*/


     // 'thinkster.accounts',
     // 'thinkster.authentication',
      //'thinkster.layout',
      //'thinkster.posts',
      //'thinkster.utils'
    ]);

    app.config(['FacebookProvider', function(FacebookProvider, $provide){
          var myAppId = '1621738971389938';
          FacebookProvider.init(myAppId);
    }]);

    /************for material preloader **********************/
    app.directive('ngMaterialPreloader', function() {
        return {
          restrict: 'A',
          link: function(scope, element, attrs) {

            element = new $.materialPreloader(scope.$eval(attrs.materialPreloader));

            element.on();
          }
        };
    });


    /***************** for social share Popup *************************/
    app.directive('sharePopup', function(){
        return{
          restrict :'AE',
          /*template:'<div><div class="socialCircle-container"> ' +
              ' <div class="socialCircle-item"><i class="fa fa-google-plus"></i></div>  ' +
              ' <div class="socialCircle-item"><i class="fa fa-clipboard"></i></div> ' +
              ' <div class="socialCircle-item"><i class="fa fa-facebook"></i></div> ' +
              ' <div class="socialCircle-item"><i class="fa fa-twitter"></i></div> ' +
              ' <div class="socialCircle-center closed"><i class="fa fa-share-alt"></i></div>' +
              '</div></div>',*/
          link : function(scope,elements,attrs){
            //alert("hello");
            $( elements ).socialCircle({
            	rotate: 0,
            	radius:70,
            	circleSize: 2,
            	speed:500
            });
          }
        }
    });

    /***************for winodw resize ***************/
    app.directive('resize', function ($window) {
        return function (scope, element, attr) {
            //alert("sdfds");
            var w = angular.element($window);
            scope.$watch(function () {
                return {
                    'h': window.innerHeight,
                    'w': window.innerWidth
                };
            }, function (newValue, oldValue) {
                //console.log(newValue, oldValue);
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;

                scope.resizeWithOffset = function (offsetH) {
                    scope.$eval(attr.notifier);
                    return {
                        'height': (newValue.h - offsetH) + 'px'
                    };
                };

            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        }
    });


    app.directive('noScroll', function(){
        return{
            restrict:'AE',
            link : function(scope, element, attrs){
                element.bind('mousewheel', function(event) {
                    //alert("helooo");
                    event.preventDefault();
                    var scrollTop = this.scrollTop;
                    this.scrollTop = (scrollTop + ((event.deltaY * event.deltaFactor) * -1));
                    //console.log(event.deltaY, event.deltaFactor, event.originalEvent.deltaMode, event.originalEvent.wheelDelta);
                });
            }
        }
    })

    //***********For set Focus on Searching Area*********
    app.directive("focus", function($state){
      var inter;
      return{
        restrict:'A',
        link:function(scope, elem, attr)
        {
          //angular.element(elem).focus();
          
        }
      }
    });

    //***********For Doctor Focus*********
    app.directive("doctorFocus", function($state){
      var inter;
      return{
        restrict:'A',
        link:function(scope, elem, attr)
        {
          elem.bind("mouseover", function(){
            elem.addClass("doctors_mouseover");
          });
          elem.bind("mouseout", function(){
            elem.removeClass("doctors_mouseover");
          });
          elem.bind("click", function(){
            $(".doctors_mouseclick").removeClass("doctors_mouseclick");
            elem.addClass("doctors_mouseclick");
          });

        }
      }
    });

    //**********Click outside of Tag then automatically Hidden field*******
    app.directive("clickOutSideTag", function(){
        return {
            restroct : 'A',
            link : function(scope, elem, attr)
            {
                elem.bind("click", function(e){
                    elem.addClass("devsite-popout-closed");
                    console.log(elem.hasClass("devsite-popout-closed"));

                });
                angular.element("body").bind("click", function(){
                    elem.removeClass("devsite-popout-closed");

                });
            }
        }
    });
    //**********For Check login or not if click any Overview left side Icon Click*****
    app.directive("checkLogin", function($timeout, CacheService, $rootScope){
      return{
        restrict : 'A',
        link : function(scope, elem, attr){
          elem.bind("click", function(event){
            if(!CacheService.isCache("client_info"))
            {
              $timeout(function(){
                $('#tile1 .loginClickClass').click();
              });
              event.preventDefault();
              event.stopPropagation();
            }
            /*else
            {
                attr.$observe('checkLogin', function(value) {
                  elem.html('<img src="'+$rootScope.static_path+'lib/image/icons/filled-star.svg"/>');
                });
            }*/
          });
        }
      }
    });

    //***************For Dynamically load Image for image mapster******
    app.directive('imageonload', function($timeout, $state, $rootScope) {
    return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                  scope.$apply(function(){
                    scope.photo.isLoading=true;
                  });

                /*console.log(attrs);
                console.log(attrs.id);
                console.log(attrs.selectImg);
                console.log(attrs.hoverImg);*/
                var selectImg =  attrs.selectImg;
                var hoverImg = attrs.hoverImg;
                    //alert(attrs.altImg);
                    $('#avatar_img').mapster({
                      mapKey: 'avatar-key',
                      render_highlight: {
                          fillColor: '2aff00',
                          stroke: false,
                          altImage: $rootScope.static_path + hoverImg
                      },
                      render_select: {
                         altImage: $rootScope.static_path + selectImg,
                         altImageOpacity: 0.9
                      },
                      singleSelect: true,
                      /*fillOpacity: 0.2,*/
                      fillColor: 'ffffff',

                      onClick:function(e){
                          $state.go($(this).attr("state-name"), {avatar_id:e.key, avatar_code:$(this).attr("avatar-code")});
                        }
                    });
                });
            }
        };
    });

    //************For go to Top of container****
    app.directive('goToTop', function(ScrollService) {
    return {
            restrict: 'A',
            scope : {
              goToTop : "@"
            },
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                  ScrollService.scrollTop(scope.goToTop);
                });
            }
        };
    });

    //************Check User Location if get then then show Specility relate to current Specility*****
    app.directive('userLocationCheck', function(CacheService, $rootScope) {
    return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function(event) {
                  if(!CacheService.isCache("user_location"))
                  {
                    scope.$apply(function(){
                      $rootScope.isUserLocationFind=false;
                    });
                    event.preventDefault();
                    event.stopPropagation();
                  }
                });
            }
        };
    });

    //****** for User Location Location Template & it's Functionlity********
    app.directive('userLocationTemplate', function($rootScope, GeoLocationService, CacheService, $interval) {
    return {
            restrict: 'EA',
            template : '<div layout="row" style="box-shadow:0.1px 0.1px 5px rgb(171, 171, 171)"><span flex="80" style="overflow:hidden; color:white; margin: auto auto;">Please Specify Your Location</span><span flex="20"><md-button ng-click="findMyCurrentLocation()" style="float: right;"><md-icon md-svg-icon="{{static_path}}lib/image/location_icon/location.svg" class="animate-location-icon"></md-icon><md-tooltip>Get Current Location</md-tooltip></md-button></span></div><div layout="column"><form name="geo_form"><md-input-container><label style="color: white;border-color: rgba(245, 31, 31, 0.12); margin-left:10px">City* (e.g. Delhi)</label><input type="text" required style="color:white; border-color: rgba(234, 229, 229, 0.95);" name="geo_city" ng-model="user_city" ng-focus="focusOnTextBox();"></md-input-container><div style="width:98%; height:200px; background:rgb(40, 40, 40); margin-top:-25px; overflow:auto" ng-class="({city_list_display_hide:isCityListDisplay, city_list_display_show:!isCityListDisplay})"><md-list class="list_view"><md-list-item ng-repeat="city in cityList | filter:user_city track by $index" ng-click="setUserCity(city.name, city.slug)"><md-button>{{city.name}}</md-button></md-list-item><md-list-item ng-show="isShowAllCities" ng-init="isShowAllCities=true"><md-button ng-click="showAllCities(); isShowAllCities=false"><a style="cursor : pointer;">Other Cities</a></md-button></md-list-item></md-list></div><md-input-container><label style="color: white;border-color: rgba(245, 31, 31, 0.12); margin-left:10px">Location(e.g. Preet Vihar)</label><input type="text" style="color:white; border-color: rgba(234, 229, 229, 0.95)" ng-model="user_location" ng-change="getLocalityList()" ng-disabled="geo_form.geo_city.$error.required"></md-input-container><div style="width:98%; height:200px; background:rgb(40, 40, 40); margin-top:-25px; overflow:auto" ng-class="({locality_list_display_hide:isLocalityListDisplay, locality_list_display_show:!isLocalityListDisplay})"><md-list class="list_view"><md-list-item ng-repeat="locality in localityList" ng-click="setUserLocality(locality.name, locality.slug)"><md-button>{{locality.name}}</md-button><md-list-item></md-list></div><md-button class="md-raised md-primary" style="width:100%; display: inline-block!important; opacity: 1!important; top: -0px!important;" ng-click="findMyLocation()" ng-disabled="!isCityListDisplay">Set Location</md-button><md-progress-linear md-mode="indeterminate" ng-show="isSearching"></md-progress-linear></form></div>',
            link: function(scope, element, attrs) {

                scope.isCityListDisplay=true;
                scope.isLocalityListDisplay=true;
                scope.cityList = [];
                var city_interval;

                var city_slug=null, location_slug=null;
                //********* Get Geo Location********
                scope.findMyCurrentLocation=function()
                {
                  scope.isSearching=true;
                  scope.$$listeners['GeoLocationEvent'] = [];
                  GeoLocationService.getCurrentLocation();
                  scope.$on("GeoLocationEvent", function($event, data){
                      
                      scope.isSearching=false;
                      scope.$apply(function(){
                          scope.user_city=data.city;
                          scope.user_location=data.locality;
                      });
                  });
                }

                //********* Set User Location When Click Set Location Button********
                scope.findMyLocation=function(){
                    var location_={
                        city : scope.user_city,
                        location : scope.user_location
                    }
                    
                    CacheService.setCache("user_location", {user_location : location_.location, user_location_slug : location_slug, user_city : location_.city, user_city_slug :city_slug});
                    $rootScope.isUserLocationFind=true;
                    scope.isShowAllCities=true;
                    $rootScope.$broadcast("LocationChangesEvent");
                }

                //********* Get City List From MySwaasth Database********

                scope.getCityList = function()
                {
                  scope.isLocalityListDisplay=true;
                  if(scope.isCityListDisplay==true)
                  {
                    if(CacheService.isCache("myswaasth_city_list"))
                    {
                      var m_city = (CacheService.getCache("myswaasth_city_list")[1]);
                      scope.cityList = m_city.major_cities;
                      scope.isCityListDisplay=false;
                    }
                    else
                    {
                      GeoLocationService.getCityList().success(function(response){
                        scope.cityList = response[1].major_cities;
                        CacheService.setCache("myswaasth_city_list", response);
                        scope.isCityListDisplay=false;
                      }).error(function(error){
                        $rootScope.alerts[0]={ type: 'danger', msg: 'Not Get City List. May be Internet Connection Failed.' };
                      });
                    }
                  }
                }
                scope.getCityList();
                scope.setUserCity = function(name, slug)
                {
                  scope.user_city=name;
                  city_slug = slug;
                  scope.isCityListDisplay=true;
                  scope.cityList = CacheService.getCache("myswaasth_city_list")[1].major_cities;
                  $interval.cancel(city_interval);
                  scope.isShowAllCities = true;
                }

                //********* When Focus on City Text Box****
                scope.focusOnTextBox = function()
                {
                  scope.user_city="";
                  scope.isCityListDisplay=false;
                  scope.isLocalityListDisplay=true;
                  scope.user_location = "";
                }

                scope.showAllCities = function()
                {
                  scope.isCityListDisplay=false;
                  var list_index = 0;
                  var cityList_ = CacheService.getCache("myswaasth_city_list")[0].cities;
                  scope.cityList = [];
                  city_interval = $interval(function(){
                    scope.cityList.push(cityList_[list_index]);
                    list_index++;
                    if(scope.cityList.length==cityList_.length)
                    {
                      $interval.cancel(city_interval);
                    }
                  }, 10);
                }
                //********* Get Locality List from MySwaasth Database********
                scope.getLocalityList = function()
                {
                  scope.isCityListDisplay=true;
                  GeoLocationService.getLocalityList(city_slug, scope.user_location).success(function(response){
                    scope.localityList = response;
                    scope.isLocalityListDisplay=false;
                  }).error(function(error){
                    $rootScope.alerts[0]={ type: 'danger', msg: 'Not Get Locality List. May be Internet Connection Failed.' };
                  });
                }
                scope.setUserLocality = function(name, slug)
                {
                  scope.user_location=name;
                  location_slug = slug;
                  scope.isLocalityListDisplay=true;
                }
            }
        };
    });

    //********* Show Loader when image loading******
    app.directive('imageLoading', function() {
    return {
            restrict: 'A',
            link: function(scope, ele, attrs) {
              ele.bind("progress", function(e){
                console.log("progress");
              });
              ele.bind("load", function(e){
                scope.$apply(function(){
                  scope.photo.isLoading=true;
                });
              });
            }
        };
    });

    //********** Date Picker Directive*******
    app.directive('datePicker', function() {
    return {
            restrict: 'A',
            link: function(scope, ele, attrs) {
              var d = new Date();
              $(ele).datepicker({dateFormat: 'yy-mm-dd', maxDate:d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()});
            }
        };
    });

    //********For Perfect ScrollBar Update****
    app.directive('scrollUpdate', function() {
    return {
            restrict: 'A',
            scope : {
              scrollUpdate : "@"
            },
            link: function(scope, ele, attrs) {
              scope.$watch('scrollUpdate', function() {
                $(ele).perfectScrollbar('update');
              });
            }
        };
    });

    //********Scroll and Fix for Book Appointment*******
    app.directive('scrollAndFix', function($timeout) {
    return {
            restrict: 'EA',
            scope : {
                  scrollerElementClass : "@",
                  showElementClass : "@"
            },
            link: function(scope, ele, attrs) {
                
                $timeout(function(){
                var scroolAndFix = angular.element(document.getElementsByClassName(scope.scrollerElementClass)[0]);
                var on_scrollshow = angular.element(document.getElementsByClassName(scope.showElementClass)[document.getElementsByClassName(scope.showElementClass).length-1]);
                
                scroolAndFix.bind("scroll", function(){
                  if(this.scrollTop > 135){
                    on_scrollshow.removeClass("fix_and_scroll_show");
                    ele.css({display:"none"});
                  }
                  else
                  {
                    on_scrollshow.addClass("fix_and_scroll_show");
                    ele.css({display:"block"});
                  }
                });
              }, 500);
            }
        };
    });
    
    //************ File Read Directive*********
    app.directive('fileRead', function() {
    return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope, ele, attrs, ngModel) {
                
                ele.bind("change", function($event){
                  scope.$apply(function(){
                    scope.myprofileoverview_vm.document=URL.createObjectURL($event.target.files[0]);
                  });

                  var reader = new FileReader();
                  reader.onload = function (loadEvent) {
                      scope.$apply(function () {
                          var send_data = loadEvent.target.result;
                      });
                  }
                  reader.readAsDataURL($event.target.files[0]);
                });
            }
        };
    });
    
    //***********For Clear all HTML data with in JSON*********
    app.filter("plainHTMLText", function(){

      return function(text)
      {
        return text.replace(/<[^>]+>/gm, '');
      }
    });

    app.filter("FilterList", function(){

      return function(input, filter_txt)
      {
        if(filter_txt=="")
        {
          return input;
        }
        else
        {
          if(input.slice(0, filter_txt.length)==filter_txt)
          {
            return input;
          }
        }
      }
    });

    //************ Filter for Parse Float to Integer***********
    app.filter("toInt", function(){

      return function(num)
      {
        return parseInt(num);
      }
    });

    //******** Filter for append <br/> after doctor, hospital e.g. (10:00 AM - 2:00 PM6:00 PM - 9:00 PM)******
    app.filter("TimeFilterBreak", function(){

      return function(time)
      {
        var time_ = time.replace(/([A-Z]+)([0-9])/g, function(p1){
          var p2 = p1.substring(0, 2)+"<br/>"+p1.substring(3, p1.length-1);
          return p2;
        });

        return time_;
      }
    });


  angular.module('myApp.config', []);

  angular.module('myApp.routes', ['ngRoute', 'ui.router','infinite-scroll', 'ngStorage', 'ui.bootstrap', 'ngSanitize', 'base64', 'directive.g+signin']).run(function($rootScope, $location, $state, ScrollService) {
      $rootScope.alerts = [
            
            ];

      $rootScope.closeAlert = function(index) {
        $rootScope.alerts.splice(index, 1);
      };
      
      $rootScope.avataranim = 'change';
      $rootScope.avatar_tab_selected = 0;

      $rootScope.location=$location;

      $rootScope.isUserLocationFind=false;
      /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
      $rootScope.static_path = 'http://127.0.0.1:8000/static/';
      $rootScope.static_server_path = 'http://52.74.163.60/';

      $rootScope.goToLeft=function(){
        ScrollService.scrollBack();
      }
      $rootScope.goToRight=function(){
        ScrollService.scrollBackword();
      }

      //********For Manually Path Copy for Share into Share Button******
      $rootScope.pathCopyForShare=function(isSuccess)
        {
            if(isSuccess)
            {
                $rootScope.alerts[0]={ type: 'success', msg: 'Data Copy into Clipboard' };
            }
            else
            {
                $rootScope.alerts[0]={ type: 'danger', msg: 'Browser not support System Clipboard. Copy link Manually' };
            }
        }

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        
        if($location.path().indexOf("/swaasth")!=-1)
        {
          angular.element("body").css("overflow", "hidden");
          $("body").scrollTop(0);
        }
        else
        {
          angular.element("body").css("overflow", "scroll");
        }
      });
  });

})();






